# 微信公众号

#### 功能列表

- 发送客服消息（文本、图片、语音、视频、音乐、图文）
- 菜单操作（查询、创建、删除、个性化菜单）
- 二维码（创建临时、永久二维码，查看二维码URL）
- 分组操作（查询、创建、修改、移动用户到分组）
- 用户信息（查询用户基本信息、获取关注者列表）
- 媒体文件（上传、获取）
- 群发消息（文本、图片、语音、视频、图文）
- 客服记录（查询客服记录，查看客服、查看在线客服）
- 群发消息
- 公众号支付（发货通知、订单查询）
- 微信小店（商品管理、库存管理、邮费模板管理、分组管理、货架管理、订单管理、功能接口）
- 模版消息
- 网址缩短
- 语义查询
- 数据分析
- JSSDK服务端支持
- 素材管理
- 摇一摇周边

设置用户备注名示例

```js
const api = new module.WechatApi('公众号的appid', '公众号的appsecret');
const result = await api.updateRemark('open_id', 'remarked'); // 设置用户备注名
```

#### api_card: API

------

##### uploadLogo

上传Logo
Examples:

```js
api.uploadLogo('filepath');
```

Result:

```js
{
 "errcode":0,
 "errmsg":"ok",
 "url":"http://mmbiz.qpic.cn/mmbiz/iaL1LJM1mF9aRKPZJkmG8xXhiaHqkKSVMMWeN3hLut7X7hicFNjakmxibMLGWpXrEXB33367o7zHN0CwngnQY7zb7g/0"
}
​``` * @name uploadLogo
```

方法签名

| 属性   | exports.uploadLogo |      |
| ---- | ------------------ | ---- |
| 参数   | filepath(String)   | 文件路径 |

##### addLocations

方法签名

| 属性   | exports.addLocations |      |
| ---- | -------------------- | ---- |
| 名字   | addLocations         |      |
| 参数   | locations(Array)     | 位置   |

##### getCardList

获取用户已领取的卡券
详细细节 https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1451025272&token=&lang=zh_CN
Examples:

```js
api.getCardList('openid', 'card_id');
```

方法签名

| 属性   | exports.getCardList |            |
| ---- | ------------------- | ---------- |
| 参数   | openid(String)      | 用户的openid  |
| 参数   | cardId(String)      | 卡券的card_id |

#### api_common: API

------

##### constructor

根据 appid 和 appsecret 创建API的构造函数
如需跨进程跨机器进行操作Wechat API（依赖access token），access token需要进行全局维护
使用策略如下：

1. 调用用户传入的获取 token 的异步方法，获得 token 之后使用
2. 使用appid/appsecret获取 token 。并调用用户传入的保存 token 方法保存
   Tips:

- 如果跨机器运行wechat模块，需要注意同步机器之间的系统时间。
  Examples:

```js
const api = new module.WechatApi('appid', 'secret');
```

以上即可满足单进程使用。
当多进程时，token 需要全局维护，以下为保存 token 的接口。

```js
const api = new module.WechatApi('appid', 'secret', async function () {
  // 传入一个获取全局 token 的方法
  return await fse.readJson('access_token.txt', 'utf8');
}, async function (token) {
  // 请将 token 存储到全局，跨进程、跨机器级别的全局，比如写到数据库、redis等
  // 这样才能在cluster模式及多机情况下使用，以下为写入到文件的示例
  await fse.outputJson('access_token.txt', token);
});
```

方法签名

| function Object() { [native code] } | constructor()            |                                |
| ----------------------------------- | ------------------------ | ------------------------------ |
| 参数                                  | appid(String)            | 在公众平台上申请得到的appid               |
| 参数                                  | appsecret(String)        | 在公众平台上申请得到的app secret          |
| 参数                                  | getToken(AsyncFunction)  | 可选的。获取全局token对象的方法，多进程模式部署时需在意 |
| 参数                                  | saveToken(AsyncFunction) | 可选的。保存全局token对象的方法，多进程模式部署时需在意 |

##### setOpts

用于设置urllib的默认options * Examples:

```js
api.setOpts({timeout: 15000});
```

方法签名

| 方法   | setOpts()    |      |
| ---- | ------------ | ---- |
| 参数   | opts(Object) | 默认选项 |

##### mixin

用于支持对象合并。将对象合并到API.prototype上，使得能够支持扩展
Examples:

```js
// 媒体管理（上传、下载）
API.mixin(require('./lib/api_media'));
```

方法签名

| 方法   | API.mixin() |        |
| ---- | ----------- | ------ |
| 参数   | obj(Object) | 要合并的对象 |

#### api_custom_service: API

------

##### getRecords

获取客服聊天记录
详细请看：http://mp.weixin.qq.com/wiki/19/7c129ec71ddfa60923ea9334557e8b23.html
Opts:

```js
{
 "starttime" : 123456789,
 "endtime" : 987654321,
 "openid": "OPENID", // 非必须
 "pagesize" : 10,
 "pageindex" : 1,
}
```

Examples:

```js
const result = await api.getRecords(opts);
```

Result:

```js
{
 "recordlist": [
   {
     "worker": " test1",
     "openid": "oDF3iY9WMaswOPWjCIp_f3Bnpljk",
     "opercode": 2002,
     "time": 1400563710,
     "text": " 您好，客服test1为您服务。"
   },
   {
     "worker": " test1",
     "openid": "oDF3iY9WMaswOPWjCIp_f3Bnpljk",
     "opercode": 2003,
     "time": 1400563731,
     "text": " 你好，有什么事情？ "
   },
 ]
}
```

方法签名

| 属性   | exports.getRecords |      |
| ---- | ------------------ | ---- |
| 参数   | opts(Object)       | 查询条件 |

##### getCustomServiceList

获取客服基本信息
详细请看：http://dkf.qq.com/document-3_1.html
Examples:

```js
const result = await api.getCustomServiceList();
```

Result:

```js
{
  "kf_list": [
    {
      "kf_account": "test1@test",
      "kf_nick": "ntest1",
      "kf_id": "1001"
    },
    {
      "kf_account": "test2@test",
      "kf_nick": "ntest2",
      "kf_id": "1002"
    },
    {
      "kf_account": "test3@test",
      "kf_nick": "ntest3",
      "kf_id": "1003"
    }
  ]
}
```

方法签名

| 属性   | exports.getCustomServiceList |
| ---- | ---------------------------- |
|      |                              |

##### getOnlineCustomServiceList

获取在线客服接待信息
详细请看：http://dkf.qq.com/document-3_2.html

Examples:

```js
const result = await api.getOnlineCustomServiceList();
```

Result:

```js
{
  "kf_online_list": [
    {
      "kf_account": "test1@test",
      "status": 1,
      "kf_id": "1001",
      "auto_accept": 0,
      "accepted_case": 1
    },
    {
      "kf_account": "test2@test",
      "status": 1,
      "kf_id": "1002",
      "auto_accept": 0,
      "accepted_case": 2
    }
  ]
}
```

方法签名

| 属性   | exports.getOnlineCustomServiceList |
| ---- | ---------------------------------- |
|      |                                    |

##### addKfAccount

添加客服账号
详细请看：http://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1458044813&token=&lang=zh_CN * Examples:

```js
const result = await api.addKfAccount('test@test', 'nickname', 'password');
```

Result:

```js
{
 "errcode" : 0,
 "errmsg" : "ok",
}
```

方法签名

| 属性   | exports.addKfAccount |                   |
| ---- | -------------------- | ----------------- |
| 参数   | account(String)      | 账号名字，格式为：前缀@公共号名字 |
| 参数   | nick(String)         | 昵称                |

##### inviteworker

邀请绑定客服帐号
详细请看：https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1458044813&token=&lang=zh_CN
Examples:

```js
const result = await api.inviteworker('test@test', 'invite_wx');
```

Result:

```js
{
 "errcode" : 0,
 "errmsg" : "ok",
}
```

方法签名

| 属性   | exports.inviteworker |                   |
| ---- | -------------------- | ----------------- |
| 参数   | account(String)      | 账号名字，格式为：前缀@公共号名字 |
| 参数   | wx(String)           | 邀请绑定的个人微信账号       |

##### updateKfAccount

设置客服账号
详细请看：http://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1458044813&token=&lang=zh_CN * Examples:

```js
api.updateKfAccount('test@test', 'nickname', 'password');
```

Result:

```js
{
 "errcode" : 0,
 "errmsg" : "ok",
}
```

方法签名

| 属性   | exports.updateKfAccount |                   |
| ---- | ----------------------- | ----------------- |
| 参数   | account(String)         | 账号名字，格式为：前缀@公共号名字 |
| 参数   | nick(String)            | 昵称                |

##### deleteKfAccount

删除客服账号
详细请看：http://mp.weixin.qq.com/wiki/9/6fff6f191ef92c126b043ada035cc935.html

Examples:

```js
api.deleteKfAccount('test@test');
```

Result:

```js
{
 "errcode" : 0,
 "errmsg" : "ok",
}
```

方法签名

| 属性   | exports.deleteKfAccount |                   |
| ---- | ----------------------- | ----------------- |
| 参数   | account(String)         | 账号名字，格式为：前缀@公共号名字 |

##### setKfAccountAvatar

设置客服头像
详细请看：http://mp.weixin.qq.com/wiki/9/6fff6f191ef92c126b043ada035cc935.html

Examples:

```js
api.setKfAccountAvatar('test@test', '/path/to/avatar.png');
```

Result:

```js
{
 "errcode" : 0,
 "errmsg" : "ok",
}
```

方法签名

| 属性   | exports.setKfAccountAvatar |                   |
| ---- | -------------------------- | ----------------- |
| 参数   | account(String)            | 账号名字，格式为：前缀@公共号名字 |
| 参数   | filepath(String)           | 头像路径              |

##### createKfSession

创建客服会话
详细请看：http://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1458044820&token=&lang=zh_CN

Examples:

```js
api.createKfSession('test@test', 'OPENID');
```

Result:

```js
{
 "errcode" : 0,
 "errmsg" : "ok",
}
```

方法签名

| 属性   | exports.createKfSession |                   |
| ---- | ----------------------- | ----------------- |
| 参数   | account(String)         | 账号名字，格式为：前缀@公共号名字 |
| 参数   | openid(String)          | openid            |

#### api_datacube: API

------

#### api_device: API

------

#### api_feedback: API

------

##### updateFeedback

标记客户的投诉处理状态
Examples:

```js
api.updateFeedback(openid, feedbackId);
```

Result:

```js
{
 "errcode": 0,
 "errmsg": "success"
}
```

方法签名

| 属性   | exports.updateFeedback |      |
| ---- | ---------------------- | ---- |
| 参数   | openid(String)         | 用户ID |
| 参数   | feedbackId(String)     | 投诉ID |

#### api_group: API

------

##### getGroups

获取分组列表
详情请见：<http://mp.weixin.qq.com/wiki/0/56d992c605a97245eb7e617854b169fc.html>
Examples:

```js
api.getGroups();
```

Result:

```js
{
 "groups": [
   {"id": 0, "name": "未分组", "count": 72596},
   {"id": 1, "name": "黑名单", "count": 36}
 ]
}
```

方法签名

| 属性   | exports.getGroups |
| ---- | ----------------- |
|      |                   |

##### getWhichGroup

查询用户在哪个分组
详情请见：<http://mp.weixin.qq.com/wiki/0/56d992c605a97245eb7e617854b169fc.html>
Examples:

```js
api.getWhichGroup(openid);
```

Result:

```js
{
 "groupid": 102
}
```

方法签名

| 属性   | exports.getWhichGroup |         |
| ---- | --------------------- | ------- |
| 参数   | openid(String)        | Open ID |

##### createGroup

创建分组
详情请见：<http://mp.weixin.qq.com/wiki/0/56d992c605a97245eb7e617854b169fc.html>
Examples:

```js
api.createGroup('groupname');
```

Result:

```js
{"group": {"id": 107, "name": "test"}}
```

方法签名

| 属性   | exports.createGroup |      |
| ---- | ------------------- | ---- |
| 参数   | name(String)        | 分组名字 |

##### updateGroup

更新分组名字
详情请见：<http://mp.weixin.qq.com/wiki/0/56d992c605a97245eb7e617854b169fc.html>
Examples:

```js
api.updateGroup(107, 'new groupname');
```

Result:

```js
{"errcode": 0, "errmsg": "ok"}
```

方法签名

| 属性   | exports.updateGroup |        |
| ---- | ------------------- | ------ |
| 参数   | id(Number)          | 分组ID   |
| 参数   | name(String)        | 新的分组名字 |

##### moveUserToGroup

移动用户进分组
详情请见：<http://mp.weixin.qq.com/wiki/0/56d992c605a97245eb7e617854b169fc.html>
Examples:

```js
api.moveUserToGroup(openid, groupId);
```

Result:

```js
{"errcode": 0, "errmsg": "ok"}
```

方法签名

| 属性   | exports.moveUserToGroup |           |
| ---- | ----------------------- | --------- |
| 参数   | openid(String)          | 用户的openid |
| 参数   | groupId(Number)         | 分组ID      |

##### moveUsersToGroup

批量移动用户分组
详情请见：<http://mp.weixin.qq.com/wiki/8/d6d33cf60bce2a2e4fb10a21be9591b8.html>
Examples:

```js
api.moveUsersToGroup(openids, groupId);
```

Result:

```js
{"errcode": 0, "errmsg": "ok"}
```

方法签名

| 属性   | exports.moveUsersToGroup |             |
| ---- | ------------------------ | ----------- |
| 参数   | openids(String)          | 用户的openid数组 |
| 参数   | groupId(Number)          | 分组ID        |

##### removeGroup

删除分组
详情请见：<http://mp.weixin.qq.com/wiki/0/56d992c605a97245eb7e617854b169fc.html>
Examples:

```js
api.removeGroup(groupId);
```

Result:

```js
{"errcode": 0, "errmsg": "ok"}
```

方法签名

| 属性   | exports.removeGroup |      |
| ---- | ------------------- | ---- |
| 参数   | groupId(Number)     | 分组ID |

#### api_ip: API

------

##### getIp

获取微信服务器IP地址
详情请见：[http://mp.weixin.qq.com/wiki/index.php?title=获取微信服务器IP地址](http://mp.weixin.qq.com/wiki/index.php?title=%E8%8E%B7%E5%8F%96%E5%BE%AE%E4%BF%A1%E6%9C%8D%E5%8A%A1%E5%99%A8IP%E5%9C%B0%E5%9D%80)
Examples:

api.getIp();

Result:

```js
{
  "ip_list":["127.0.0.1","127.0.0.1"]
}
```

方法签名

| 属性   | exports.getIp |
| ---- | ------------- |
|      |               |

#### api_js: API

------

##### registerTicketHandle

多台服务器负载均衡时，ticketToken需要外部存储共享。
需要调用此registerTicketHandle来设置获取和保存的自定义方法。 * Examples:

```js
api.registerTicketHandle(getTicketToken, saveTicketToken);
// getTicketToken
function getTicketToken(type) {
 settingModel.getItem(type, {key: 'weixin_ticketToken'}, function (err, setting) {
   if (err) return callback(err);
   callback(null, setting.value);
 });
}
// saveTicketToken
function saveTicketToken(type, _ticketToken) {
 settingModel.setItem(type, {key:'weixin_ticketToken', value: ticketToken}, function (err) {
   if (err) return callback(err);
   callback(null);
 });
}
```

方法签名

| 方法   | exports.registerTicketHandle() |                    |
| ---- | ------------------------------ | ------------------ |
| 参数   | getTicketToken(Function)       | 获取外部ticketToken的函数 |
| 参数   | saveTicketToken(Function)      | 存储外部ticketToken的函数 |

##### getTicket

获取js sdk所需的有效js ticket

- `err`, 异常对象
- `result`, 正常获取时的数据 * Result:
- `errcode`, 0为成功
- `errmsg`, 成功为'ok'，错误则为详细错误信息
- `ticket`, js sdk有效票据，如：bxLdikRXVbTPdHSM05e5u5sUoXNKd8-41ZO3MhKoyN5OfkWITDGgnr2fwJ0m9E8NYzWKVZvdVtaUgWvsdshFKA
- `expires_in`, 有效期7200秒，开发者必须在自己的服务全局缓存jsapi_ticket

方法签名

| 属性   | exports.getTicket |
| ---- | ----------------- |
|      |                   |

##### getJsConfig

获取微信JS SDK Config的所需参数 * Examples:

```js
var param = {
 debug: false,
 jsApiList: ['onMenuShareTimeline', 'onMenuShareAppMessage'],
 url: 'http://www.xxx.com'
};
api.getJsConfig(param);
```

- `result`, 调用正常时得到的js sdk config所需参数

方法签名

| 属性   | exports.getJsConfig |      |
| ---- | ------------------- | ---- |
| 参数   | param(Object)       | 参数   |

##### getCardExt

获取微信JS SDK Config的所需参数
Examples:

```js
const param = {
 card_id: 'p-hXXXXXXX',
 code: '1234',
 openid: '111111',
 balance: 100
};
api.getCardExt(param);
```

- `result`, 调用正常时得到的card_ext对象，包含所需参数

方法签名

| 属性   | exports.getCardExt |      |
| ---- | ------------------ | ---- |
| 名字   | getCardExt         |      |
| 参数   | param(Object)      | 参数   |

##### getLatestTicket

获取最新的js api ticket
Examples:

```js
api.getLatestTicket();
```

- `err`, 获取js api ticket出现异常时的异常对象
- `ticket`, 获取的ticket

方法签名

| 属性   | exports.getLatestTicket |
| ---- | ----------------------- |
|      |                         |

#### api_mass_send: API

------

##### uploadNews

上传多媒体文件，分别有图片（image）、语音（voice）、视频（video）和缩略图（thumb）
详情请见：<http://mp.weixin.qq.com/wiki/15/5380a4e6f02f2ffdc7981a8ed7a40753.html>
Examples:

```js
api.uploadNews(news);
```

News:

```js
{
 "articles": [
   {
     "thumb_media_id":"qI6_Ze_6PtV7svjolgs-rN6stStuHIjs9_DidOHaj0Q-mwvBelOXCFZiq2OsIU-p",
     "author":"xxx",
     "title":"Happy Day",
     "content_source_url":"www.qq.com",
     "content":"content",
     "digest":"digest",
     "show_cover_pic":"1"
  },
  {
     "thumb_media_id":"qI6_Ze_6PtV7svjolgs-rN6stStuHIjs9_DidOHaj0Q-mwvBelOXCFZiq2OsIU-p",
     "author":"xxx",
     "title":"Happy Day",
     "content_source_url":"www.qq.com",
     "content":"content",
     "digest":"digest",
     "show_cover_pic":"0"
  }
 ]
}
```

Result:

```js
{
 "type":"news",
 "media_id":"CsEf3ldqkAYJAU6EJeIkStVDSvffUJ54vqbThMgplD-VJXXof6ctX5fI6-aYyUiQ",
 "created_at":1391857799
}
​``` * @param {Object} news 图文消息对象
```

方法签名

| 属性   | exports.uploadNews |
| ---- | ------------------ |
|      |                    |

##### uploadMPVideo

将通过上传下载多媒体文件得到的视频media_id变成视频素材
详情请见：<http://mp.weixin.qq.com/wiki/15/5380a4e6f02f2ffdc7981a8ed7a40753.html>
Examples:

```js
api.uploadMPVideo(opts);
```

Opts:

```js
{
 "media_id": "rF4UdIMfYK3efUfyoddYRMU50zMiRmmt_l0kszupYh_SzrcW5Gaheq05p_lHuOTQ",
 "title": "TITLE",
 "description": "Description"
}
```

Result:

```js
{
 "type":"video",
 "media_id":"IhdaAQXuvJtGzwwc0abfXnzeezfO0NgPK6AQYShD8RQYMTtfzbLdBIQkQziv2XJc",
 "created_at":1391857799
}
​``` * @param {Object} opts 待上传为素材的视频
```

方法签名

| 属性   | exports.uploadMPVideo |
| ---- | --------------------- |
|      |                       |

##### massSend

群发消息，分别有图文（news）、文本(text)、语音（voice）、图片（image）和视频（video）
详情请见：<http://mp.weixin.qq.com/wiki/15/5380a4e6f02f2ffdc7981a8ed7a40753.html>
Examples:

```js
api.massSend(opts, receivers);
```

opts:

```js
{
 "image":{
   "media_id":"123dsdajkasd231jhksad"
 },
 "msgtype":"image"
}
```

Result:

```js
{
 "errcode":0,
 "errmsg":"send job submission success",
 "msg_id":34182
}
​``` * @param {Object} opts 待发送的数据
```

方法签名

| 属性   | exports.massSend        |                    |
| ---- | ----------------------- | ------------------ |
| 参数   | receivers(String,Array) | 接收人。一个组，或者openid列表 |

##### massSendNews

群发图文（news）消息
详情请见：<http://mp.weixin.qq.com/wiki/15/5380a4e6f02f2ffdc7981a8ed7a40753.html>
Examples:

```js
api.massSendNews(mediaId, receivers);
```

Result:

```js
{
 "errcode":0,
 "errmsg":"send job submission success",
 "msg_id":34182
}
​``` * @param {String} mediaId 图文消息的media id
```

方法签名

| 属性   | exports.massSendNews            |                                    |
| ---- | ------------------------------- | ---------------------------------- |
| 参数   | receivers(String,Array,Boolean) | 接收人。一个组，或者openid列表, 或者true（群发给所有人） |

##### massSendText

群发文字（text）消息
详情请见：<http://mp.weixin.qq.com/wiki/15/5380a4e6f02f2ffdc7981a8ed7a40753.html>
Examples:

```js
api.massSendText(content, receivers);
```

Result:

```js
{
 "errcode":0,
 "errmsg":"send job submission success",
 "msg_id":34182
}
​``` * @param {String} content 文字消息内容
```

方法签名

| 属性   | exports.massSendText    |                    |
| ---- | ----------------------- | ------------------ |
| 参数   | receivers(String,Array) | 接收人。一个组，或者openid列表 |

##### massSendVoice

群发声音（voice）消息
详情请见：<http://mp.weixin.qq.com/wiki/15/5380a4e6f02f2ffdc7981a8ed7a40753.html>
Examples:

```js
api.massSendVoice(media_id, receivers);
```

Result:

```js
{
 "errcode":0,
 "errmsg":"send job submission success",
 "msg_id":34182
}
​``` * @param {String} mediaId 声音media id
```

方法签名

| 属性   | exports.massSendVoice   |                    |
| ---- | ----------------------- | ------------------ |
| 参数   | receivers(String,Array) | 接收人。一个组，或者openid列表 |

##### massSendImage

群发图片（image）消息
详情请见：<http://mp.weixin.qq.com/wiki/15/5380a4e6f02f2ffdc7981a8ed7a40753.html>
Examples:

```js
api.massSendImage(media_id, receivers);
```

Result:

```js
{
 "errcode":0,
 "errmsg":"send job submission success",
 "msg_id":34182
}
​``` * @param {String} mediaId 图片media id
```

方法签名

| 属性   | exports.massSendImage   |                    |
| ---- | ----------------------- | ------------------ |
| 参数   | receivers(String,Array) | 接收人。一个组，或者openid列表 |

##### massSendVideo

群发视频（video）消息
详情请见：<http://mp.weixin.qq.com/wiki/15/5380a4e6f02f2ffdc7981a8ed7a40753.html>
Examples:

```js
api.massSendVideo(mediaId, receivers);
```

Result:

```js
{
 "errcode":0,
 "errmsg":"send job submission success",
 "msg_id":34182
}
​``` * @param {String} mediaId 视频media id
```

方法签名

| 属性   | exports.massSendVideo   |                    |
| ---- | ----------------------- | ------------------ |
| 参数   | receivers(String,Array) | 接收人。一个组，或者openid列表 |

##### massSendMPVideo

群发视频（video）消息，直接通过上传文件得到的media id进行群发（自动生成素材）
详情请见：<http://mp.weixin.qq.com/wiki/15/5380a4e6f02f2ffdc7981a8ed7a40753.html>
Examples:

```js
api.massSendMPVideo(data, receivers);
```

Data:

```js
{
 "media_id": "rF4UdIMfYK3efUfyoddYRMU50zMiRmmt_l0kszupYh_SzrcW5Gaheq05p_lHuOTQ",
 "title": "TITLE",
 "description": "Description"
}
```

Result:

```js
{
 "errcode":0,
 "errmsg":"send job submission success",
 "msg_id":34182
}
​``` * @param {Object} data 视频数据
```

方法签名

| 属性   | exports.massSendMPVideo |                    |
| ---- | ----------------------- | ------------------ |
| 参数   | receivers(String,Array) | 接收人。一个组，或者openid列表 |

##### deleteMass

删除群发消息
详情请见：<http://mp.weixin.qq.com/wiki/15/5380a4e6f02f2ffdc7981a8ed7a40753.html>
Examples:

```js
api.deleteMass(message_id);
```

Result:

```js
{
 "errcode":0,
 "errmsg":"ok"
}
​``` * @param {String} messageId 待删除群发的消息id
```

方法签名

| 属性   | exports.deleteMass |
| ---- | ------------------ |
|      |                    |

##### previewNews

预览接口，预览图文消息
详情请见：<http://mp.weixin.qq.com/wiki/15/5380a4e6f02f2ffdc7981a8ed7a40753.html>
Examples:

```js
api.previewNews(openid, mediaId);
```

Result:

```js
{
 "errcode":0,
 "errmsg":"send job submission success",
 "msg_id": 34182
}
​``` * @param {String} openid 用户openid
```

方法签名

| 属性   | exports.previewNews |             |
| ---- | ------------------- | ----------- |
| 参数   | mediaId(String)     | 图文消息mediaId |

##### previewText

预览接口，预览文本消息
详情请见：<http://mp.weixin.qq.com/wiki/15/5380a4e6f02f2ffdc7981a8ed7a40753.html>
Examples:

```js
api.previewText(openid, content);
```

Result:

```js
{
 "errcode":0,
 "errmsg":"send job submission success",
 "msg_id": 34182
}
```

方法签名

| 属性   | exports.previewText |          |
| ---- | ------------------- | -------- |
| 参数   | openid(String)      | 用户openid |
| 参数   | content(String)     | 文本消息     |

##### previewVoice

预览接口，预览语音消息
详情请见：<http://mp.weixin.qq.com/wiki/15/5380a4e6f02f2ffdc7981a8ed7a40753.html>
Examples:

```js
api.previewVoice(openid, mediaId);
```

Result:

```js
{
 "errcode":0,
 "errmsg":"send job submission success",
 "msg_id": 34182
}
​``` * @param {String} openid 用户openid
```

方法签名

| 属性   | exports.previewVoice |           |
| ---- | -------------------- | --------- |
| 参数   | mediaId(String)      | 语音mediaId |

##### previewImage

预览接口，预览图片消息
详情请见：<http://mp.weixin.qq.com/wiki/15/5380a4e6f02f2ffdc7981a8ed7a40753.html>
Examples:

```
api.previewImage(openid, mediaId);
```

Result:

```js
{
 "errcode":0,
 "errmsg":"send job submission success",
 "msg_id": 34182
}
​``` * @param {String} openid 用户openid
```

方法签名

| 属性   | exports.previewImage |           |
| ---- | -------------------- | --------- |
| 参数   | mediaId(String)      | 图片mediaId |

##### previewVideo

预览接口，预览视频消息
详情请见：<http://mp.weixin.qq.com/wiki/15/5380a4e6f02f2ffdc7981a8ed7a40753.html>
Examples:

```js
api.previewVideo(openid, mediaId);
```

Result:

```js
{
 "errcode":0,
 "errmsg":"send job submission success",
 "msg_id": 34182
}
​``` * @param {String} openid 用户openid
```

方法签名

| 属性   | exports.previewVideo |           |
| ---- | -------------------- | --------- |
| 参数   | mediaId(String)      | 视频mediaId |

##### getMassMessageStatus

查询群发消息状态
详情请见：<http://mp.weixin.qq.com/wiki/15/5380a4e6f02f2ffdc7981a8ed7a40753.html>
Examples:

```js
api.getMassMessageStatus(messageId);
```

Result:

```js
{
 "msg_id":201053012,
 "msg_status":"SEND_SUCCESS"
}
​``` * @param {String} messageId 消息ID
```

方法签名

| 属性   | exports.getMassMessageStatus |
| ---- | ---------------------------- |
|      |                              |

#### api_material: API

------

##### uploadMaterial

上传永久素材，分别有图片（image）、语音（voice）、和缩略图（thumb）
详情请见：<http://mp.weixin.qq.com/wiki/14/7e6c03263063f4813141c3e17dd4350a.html>
Examples:

```js
api.uploadMaterial('filepath', type);
```

Result:

```js
{"type":"TYPE","media_id":"MEDIA_ID","created_at":123456789}
```

Shortcut: * - `exports.uploadImageMaterial(filepath);`

- `exports.uploadVoiceMaterial(filepath);`
- `exports.uploadThumbMaterial(filepath);` * @param {String} filepath 文件路径

方法签名

| 属性   | exports.uploadMaterial |                                  |
| ---- | ---------------------- | -------------------------------- |
| 参数   | type(String)           | 媒体类型，可用值有image、voice、video、thumb |

##### uploadVideoMaterial

上传永久素材，视频（video）
详情请见：<http://mp.weixin.qq.com/wiki/14/7e6c03263063f4813141c3e17dd4350a.html>
Examples:

```js
const description = {
  "title":VIDEO_TITLE,
  "introduction":INTRODUCTION
};
api.uploadVideoMaterial('filepath', description);
```

Result:

```js
{"media_id":"MEDIA_ID"}
```

方法签名

| 属性   | exports.uploadVideoMaterial |        |
| ---- | --------------------------- | ------ |
| 参数   | filepath(String)            | 视频文件路径 |
| 参数   | description(Object)         | 描述     |

##### uploadNewsMaterial

新增永久图文素材 * News:

```js
{
 "articles": [
   {
     "title": TITLE,
     "thumb_media_id": THUMB_MEDIA_ID,
     "author": AUTHOR,
     "digest": DIGEST,
     "show_cover_pic": SHOW_COVER_PIC(0 / 1),
     "content": CONTENT,
     "content_source_url": CONTENT_SOURCE_URL
   },
   //若新增的是多图文素材，则此处应还有几段articles结构
 ]
}
```

Examples:

```js
api.uploadNewsMaterial(news);
```

Result:

```js
{"errcode":0,"errmsg":"ok"}
```

方法签名

| 属性   | exports.uploadNewsMaterial |      |
| ---- | -------------------------- | ---- |
| 参数   | news(Object)               | 图文对象 |

##### updateNewsMaterial

更新永久图文素材
News:

```js
{
 "media_id":MEDIA_ID,
 "index":INDEX,
 "articles": [
   {
     "title": TITLE,
     "thumb_media_id": THUMB_MEDIA_ID,
     "author": AUTHOR,
     "digest": DIGEST,
     "show_cover_pic": SHOW_COVER_PIC(0 / 1),
     "content": CONTENT,
     "content_source_url": CONTENT_SOURCE_URL
   },
   //若新增的是多图文素材，则此处应还有几段articles结构
 ]
}
```

Examples:

```js
api.uploadNewsMaterial(news);
```

Result:

```js
{"errcode":0,"errmsg":"ok"}
```

方法签名

| 属性   | exports.updateNewsMaterial |      |
| ---- | -------------------------- | ---- |
| 参数   | news(Object)               | 图文对象 |

##### getMaterial

根据媒体ID获取永久素材
详情请见：<http://mp.weixin.qq.com/wiki/4/b3546879f07623cb30df9ca0e420a5d0.html>
Examples:

```js
api.getMaterial('media_id');
```

- `result`, 调用正常时得到的文件Buffer对象
- `res`, HTTP响应对象 * @param {String} mediaId 媒体文件的ID

方法签名

| 属性   | exports.getMaterial |
| ---- | ------------------- |
|      |                     |

##### removeMaterial

删除永久素材
详情请见：<http://mp.weixin.qq.com/wiki/5/e66f61c303db51a6c0f90f46b15af5f5.html>
Examples:

```js
api.removeMaterial('media_id');
```

- `result`, 调用正常时得到的文件Buffer对象
- `res`, HTTP响应对象 * @param {String} mediaId 媒体文件的ID

方法签名

| 属性   | exports.removeMaterial |
| ---- | ---------------------- |
|      |                        |

##### getMaterialCount

获取素材总数
详情请见：<http://mp.weixin.qq.com/wiki/16/8cc64f8c189674b421bee3ed403993b8.html>
Examples:

```js
api.getMaterialCount();
```

- `result`, 调用正常时得到的文件Buffer对象
- `res`, HTTP响应对象 * Result:

```js
{
 "voice_count":COUNT,
 "video_count":COUNT,
 "image_count":COUNT,
 "news_count":COUNT
}
```

方法签名

| 属性   | exports.getMaterialCount |
| ---- | ------------------------ |
|      |                          |

##### getMaterials

获取永久素材列表
详情请见：<http://mp.weixin.qq.com/wiki/12/2108cd7aafff7f388f41f37efa710204.html>
Examples:

```js
api.getMaterials(type, offset, count);
```

- `result`, 调用正常时得到的文件Buffer对象
- `res`, HTTP响应对象 * Result:

```js
{
 "total_count": TOTAL_COUNT,
 "item_count": ITEM_COUNT,
 "item": [{
   "media_id": MEDIA_ID,
   "name": NAME,
   "update_time": UPDATE_TIME
 },
 //可能会有多个素材
 ]
}
```

方法签名

| 属性   | exports.getMaterials |                                          |
| ---- | -------------------- | ---------------------------------------- |
| 参数   | type(String)         | 素材的类型，图片（image）、视频（video）、语音 （voice）、图文（news） |
| 参数   | offset(Number)       | 从全部素材的该偏移位置开始返回，0表示从第一个素材 返回             |
| 参数   | count(Number)        | 返回素材的数量，取值在1到20之间                        |

#### api_media: API

------

##### uploadMedia

新增临时素材，分别有图片（image）、语音（voice）、视频（video）和缩略图（thumb）
详情请见：<http://mp.weixin.qq.com/wiki/5/963fc70b80dc75483a271298a76a8d59.html>
Examples:

```js
api.uploadMedia('filepath', type);
```

Result:

```js
{"type":"TYPE","media_id":"MEDIA_ID","created_at":123456789}
```

Shortcut:

- `exports.uploadImageMedia(filepath);`
- `exports.uploadVoiceMedia(filepath);`
- `exports.uploadVideoMedia(filepath);`
- `exports.uploadThumbMedia(filepath);`

不再推荐使用：

- `exports.uploadImage(filepath);`
- `exports.uploadVoice(filepath);`
- `exports.uploadVideo(filepath);`
- `exports.uploadThumb(filepath);`

方法签名

| 属性   | exports.uploadMedia     |                                  |
| ---- | ----------------------- | -------------------------------- |
| 参数   | filepath(String,Buffer) | 文件路径/文件Buffer数据                  |
| 参数   | type(String)            | 媒体类型，可用值有image、voice、video、thumb |
| 参数   | filename(String)        | 文件名                              |
| 参数   | mime(String)            | 文件类型,filepath为Buffer数据时才需要传      |

##### getMedia

获取临时素材
详情请见：<http://mp.weixin.qq.com/wiki/11/07b6b76a6b6e8848e855a435d5e34a5f.html>
Examples:

```js
api.getMedia('media_id');
```

- `result`, 调用正常时得到的文件Buffer对象
- `res`, HTTP响应对象

方法签名

| 属性   | exports.getMedia |         |
| ---- | ---------------- | ------- |
| 参数   | mediaId(String)  | 媒体文件的ID |

##### uploadImage

上传图文消息内的图片获取URL
详情请见：<http://mp.weixin.qq.com/wiki/15/5380a4e6f02f2ffdc7981a8ed7a40753.html>
Examples:

```js
api.uploadImage('filepath');
```

Result:

```js
{"url":  "http://mmbiz.qpic.cn/mmbiz/gLO17UPS6FS2xsypf378iaNhWacZ1G1UplZYWEYfwvuU6Ont96b1roYsCNFwaRrSaKTPCUdBK9DgEHicsKwWCBRQ/0"}
```

方法签名

| 属性   | exports.uploadImage |        |
| ---- | ------------------- | ------ |
| 参数   | filepath(String)    | 图片文件路径 |

#### api_menu: API

------

##### createMenu

创建自定义菜单
详细请看：http://mp.weixin.qq.com/wiki/index.php?title=自定义菜单创建接口 * Menu:

```js
{
 "button":[
   {
     "type":"click",
     "name":"今日歌曲",
     "key":"V1001_TODAY_MUSIC"
   },
   {
     "name":"菜单",
     "sub_button":[
       {
         "type":"view",
         "name":"搜索",
         "url":"http://www.soso.com/"
       },
       {
         "type":"click",
         "name":"赞一下我们",
         "key":"V1001_GOOD"
       }
     ]
    }
 ]
}
```

Examples:

```js
const result = await api.createMenu(menu);
```

Result:

```js
{"errcode":0,"errmsg":"ok"}
```

方法签名

| 属性   | exports.createMenu |      |
| ---- | ------------------ | ---- |
| 参数   | menu(Object)       | 菜单对象 |

##### getMenu

获取菜单
详细请看：[http://mp.weixin.qq.com/wiki/index.php?title=自定义菜单查询接口](http://mp.weixin.qq.com/wiki/index.php?title=%E8%87%AA%E5%AE%9A%E4%B9%89%E8%8F%9C%E5%8D%95%E6%9F%A5%E8%AF%A2%E6%8E%A5%E5%8F%A3)

Examples:

```js
const result = await api.getMenu();
```

Result:

```js
// 结果示例
{
 "menu": {
   "button":[
     {"type":"click","name":"今日歌曲","key":"V1001_TODAY_MUSIC","sub_button":[]},
     {"type":"click","name":"歌手简介","key":"V1001_TODAY_SINGER","sub_button":[]},
     {"name":"菜单","sub_button":[
       {"type":"view","name":"搜索","url":"http://www.soso.com/","sub_button":[]},
       {"type":"view","name":"视频","url":"http://v.qq.com/","sub_button":[]},
       {"type":"click","name":"赞一下我们","key":"V1001_GOOD","sub_button":[]}]
     }
   ]
 }
}
```

方法签名

| 属性   | exports.getMenu |
| ---- | --------------- |
|      |                 |

##### removeMenu

删除自定义菜单
详细请看：[http://mp.weixin.qq.com/wiki/index.php?title=自定义菜单删除接口](http://mp.weixin.qq.com/wiki/index.php?title=%E8%87%AA%E5%AE%9A%E4%B9%89%E8%8F%9C%E5%8D%95%E5%88%A0%E9%99%A4%E6%8E%A5%E5%8F%A3)
Examples:

```js
const result = await api.removeMenu();
```

Result:

```js
{"errcode":0,"errmsg":"ok"}
```

方法签名

| 属性   | exports.removeMenu |
| ---- | ------------------ |
|      |                    |

##### getMenuConfig

获取自定义菜单配置
详细请看：<http://mp.weixin.qq.com/wiki/17/4dc4b0514fdad7a5fbbd477aa9aab5ed.html>
Examples:

```js
const result = await api.getMenuConfig();
```

Result:

```js
{"errcode":0,"errmsg":"ok"}
```

方法签名

| 属性   | exports.getMenuConfig |
| ---- | --------------------- |
|      |                       |

##### addConditionalMenu

创建个性化自定义菜单
详细请看：http://mp.weixin.qq.com/wiki/0/c48ccd12b69ae023159b4bfaa7c39c20.html * Menu:

```js
{
 "button":[
 {
     "type":"click",
     "name":"今日歌曲",
     "key":"V1001_TODAY_MUSIC"
 },
 {
   "name":"菜单",
   "sub_button":[
   {
     "type":"view",
     "name":"搜索",
     "url":"http://www.soso.com/"
   },
   {
     "type":"view",
     "name":"视频",
     "url":"http://v.qq.com/"
   },
   {
     "type":"click",
     "name":"赞一下我们",
     "key":"V1001_GOOD"
   }]
}],
"matchrule":{
 "group_id":"2",
 "sex":"1",
 "country":"中国",
 "province":"广东",
 "city":"广州",
 "client_platform_type":"2"
 }
}
```

Examples:

```
const result = await api.addConditionalMenu(menu);
```

Result:

```
{"errcode":0,"errmsg":"ok"}
```

方法签名

| 属性   | exports.addConditionalMenu |      |
| ---- | -------------------------- | ---- |
| 参数   | menu(Object)               | 菜单对象 |

##### delConditionalMenu

删除个性化自定义菜单
详细请看：http://mp.weixin.qq.com/wiki/0/c48ccd12b69ae023159b4bfaa7c39c20.html

Menu:

```js
{
 "menuid":"208379533"
}
```

Examples:

```js
const result = await api.delConditionalMenu(menuid);
```

Result:

```js
{"errcode":0,"errmsg":"ok"}
```

方法签名

| 属性   | exports.delConditionalMenu |      |
| ---- | -------------------------- | ---- |
| 参数   | menuid(String)             | 菜单id |

##### tryConditionalMenu

测试个性化自定义菜单
详细请看：http://mp.weixin.qq.com/wiki/0/c48ccd12b69ae023159b4bfaa7c39c20.html

Menu:

```js
{
 "user_id":"nickma"
}
```

Examples:

```js
const result = await api.tryConditionalMenu(user_id);
```

Result:

```js
{
   "button": [
       {
           "type": "view",
           "name": "tx",
           "url": "http://www.qq.com/",
           "sub_button": [ ]
       },
       {
           "type": "view",
           "name": "tx",
           "url": "http://www.qq.com/",
           "sub_button": [ ]
       },
       {
           "type": "view",
           "name": "tx",
           "url": "http://www.qq.com/",
           "sub_button": [ ]
       }
   ]
}
```

方法签名

| 属性   | exports.tryConditionalMenu |                                 |
| ---- | -------------------------- | ------------------------------- |
| 参数   | user_id(String)            | user_id可以是粉丝的OpenID，也可以是粉丝的微信号。 |

#### api_message: API

------

##### sendText

客服消息，发送文字消息
详细细节 http://mp.weixin.qq.com/wiki/index.php?title=发送客服消息
Examples:

```js
api.sendText('openid', 'Hello world');
```

方法签名

| 属性   | exports.sendText |           |
| ---- | ---------------- | --------- |
| 参数   | openid(String)   | 用户的openid |
| 参数   | text(String)     | 发送的消息内容   |

##### sendImage

客服消息，发送图片消息
详细细节 http://mp.weixin.qq.com/wiki/index.php?title=发送客服消息
Examples:

```js
api.sendImage('openid', 'media_id');
```

方法签名

| 属性   | exports.sendImage |                         |
| ---- | ----------------- | ----------------------- |
| 参数   | openid(String)    | 用户的openid               |
| 参数   | mediaId(String)   | 媒体文件的ID，参见uploadMedia方法 |

##### sendCard

客服消息，发送卡券
详细细节 http://mp.weixin.qq.com/wiki/index.php?title=发送客服消息
Examples:

```js
api.sendCard('openid', 'card_id');
```

方法签名

| 属性   | exports.sendCard |           |
| ---- | ---------------- | --------- |
| 参数   | openid(String)   | 用户的openid |
| 参数   | card_id(String)  | 卡券的ID     |

##### sendVoice

客服消息，发送语音消息
详细细节 http://mp.weixin.qq.com/wiki/index.php?title=发送客服消息
Examples:

```js
api.sendVoice('openid', 'media_id');
```

方法签名

| 属性   | exports.sendVoice |           |
| ---- | ----------------- | --------- |
| 参数   | openid(String)    | 用户的openid |
| 参数   | mediaId(String)   | 媒体文件的ID   |

##### sendVideo

客服消息，发送视频消息
详细细节 http://mp.weixin.qq.com/wiki/index.php?title=发送客服消息
Examples:

```js
api.sendVideo('openid', 'media_id', 'thumb_media_id');
```

方法签名

| 属性   | exports.sendVideo    |           |
| ---- | -------------------- | --------- |
| 参数   | openid(String)       | 用户的openid |
| 参数   | mediaId(String)      | 媒体文件的ID   |
| 参数   | thumbMediaId(String) | 缩略图文件的ID  |

##### sendMusic

客服消息，发送音乐消息
详细细节 http://mp.weixin.qq.com/wiki/index.php?title=发送客服消息
Examples:

```js
const music = {
 title: '音乐标题', // 可选
 description: '描述内容', // 可选
 musicurl: 'http://url.cn/xxx', 音乐文件地址
 hqmusicurl: "HQ_MUSIC_URL",
 thumb_media_id: "THUMB_MEDIA_ID"
};
api.sendMusic('openid', music);
```

方法签名

| 属性   | exports.sendMusic |           |
| ---- | ----------------- | --------- |
| 参数   | openid(String)    | 用户的openid |
| 参数   | music(Object)     | 音乐文件      |

##### sendNews

客服消息，发送图文消息
详细细节 http://mp.weixin.qq.com/wiki/index.php?title=发送客服消息
Examples:

```js
const articles = [
 {
   "title":"Happy Day",
   "description":"Is Really A Happy Day",
   "url":"URL",
   "picurl":"PIC_URL"
 },
 {
   "title":"Happy Day",
   "description":"Is Really A Happy Day",
   "url":"URL",
   "picurl":"PIC_URL"
 }];
api.sendNews('openid', articles);
```

方法签名

| 属性   | exports.sendNews |           |
| ---- | ---------------- | --------- |
| 参数   | openid(String)   | 用户的openid |
| 参数   | articles(Array)  | 图文列表      |

##### getAutoreply

获取自动回复规则
详细请看：<http://mp.weixin.qq.com/wiki/19/ce8afc8ae7470a0d7205322f46a02647.html>
Examples:

```js
const result = await api.getAutoreply();
```

Result:

```js
{
"is_add_friend_reply_open": 1,
"is_autoreply_open": 1,
"add_friend_autoreply_info": {
    "type": "text",
    "content": "Thanks for your attention!"
},
"message_default_autoreply_info": {
    "type": "text",
    "content": "Hello, this is autoreply!"
},
"keyword_autoreply_info": {
    "list": [
        {
            "rule_name": "autoreply-news",
            "create_time": 1423028166,
            "reply_mode": "reply_all",
            "keyword_list_info": [
                {
                    "type": "text",
                    "match_mode": "contain",
                    "content": "news测试"//此处content即为关键词内容
                }
            ],
            "reply_list_info": [
                {
                    "type": "news",
                    "news_info": {
                        "list": [
                            {
                                "title": "it's news",
                                "author": "jim",
                                "digest": "it's digest",
                                "show_cover": 1,
                                "cover_url": "http://mmbiz.qpic.cn/mmbiz/GE7et87vE9vicuCibqXsX9GPPLuEtBfXfKbE8sWdt2DDcL0dMfQWJWTVn1N8DxI0gcRmrtqBOuwQHeuPKmFLK0ZQ/0",
                                "content_url": "http://mp.weixin.qq.com/s?__biz=MjM5ODUwNTM3Ng==&mid=203929886&idx=1&sn=628f964cf0c6d84c026881b6959aea8b#rd",
                                "source_url": "http://www.url.com"
                            }
                        ]
                    }
                },
                {
                    ....
                }
            ]
        },
        {
            "rule_name": "autoreply-voice",
            "create_time": 1423027971,
            "reply_mode": "random_one",
            "keyword_list_info": [
                {
                    "type": "text",
                    "match_mode": "contain",
                    "content": "voice测试"
                }
            ],
            "reply_list_info": [
                {
                    "type": "voice",
                    "content": "NESsxgHEvAcg3egJTtYj4uG1PTL6iPhratdWKDLAXYErhN6oEEfMdVyblWtBY5vp"
                }
            ]
        },
        ...
    ]
}
}
```

方法签名

| 属性   | exports.getAutoreply |
| ---- | -------------------- |
|      |                      |

#### api_payment: API

------

##### deliverNotify

微信公众号支付: 发货通知
详情请见：<http://mp.weixin.qq.com/htmledition/res/bussiness-faq/wx_mp_pay.zip> 接口文档订单发货通知 * Data:

```js
{
  "appid" : "wwwwb4f85f3a797777",
  "openid" : "oX99MDgNcgwnz3zFN3DNmo8uwa-w",
  "transid" : "111112222233333",
  "out_trade_no" : "555666uuu",
  "deliver_timestamp" : "1369745073",
  "deliver_status" : "1",
  "deliver_msg" : "ok",
  "app_signature" : "53cca9d47b883bd4a5c85a9300df3da0cb48565c",
  "sign_method" : "sha1"
}
```

Examples:

```js
api.deliverNotify(data);
```

Result:

```js
{"errcode":0, "errmsg":"ok"}
​``` * @param {Object} package package对象
```

方法签名

| 属性   | exports.deliverNotify |
| ---- | --------------------- |
|      |                       |

##### orderQuery

微信公众号支付: 订单查询
详情请见：<http://mp.weixin.qq.com/htmledition/res/bussiness-faq/wx_mp_pay.zip> 接口文档订单查询部分 * Package:

```js
{
  "appid" : "wwwwb4f85f3a797777",
  "package" : "out_trade_no=11122&partner=1900090055&sign=4e8d0df3da0c3d0df38f",
  "timestamp" : "1369745073",
  "app_signature" : "53cca9d47b883bd4a5c85a9300df3da0cb48565c",
  "sign_method" : "sha1"
}
```

Examples:

```js
api.orderQuery(query);
```

Result:

```js
{
  "errcode":0,
  "errmsg":"ok",
  "order_info": {
    "ret_code":0,
    "ret_msg":"",
    "input_charset":"GBK",
    "trade_state":"0",
    "trade_mode":"1",
    "partner":"1900000109",
    "bank_type":"CMB_FP",
    "bank_billno":"207029722724",
    "total_fee":"1",
    "fee_type":"1",
    "transaction_id":"1900000109201307020305773741",
    "out_trade_no":"2986872580246457300",
    "is_split":"false",
    "is_refund":"false",
    "attach":"",
    "time_end":"20130702175943",
    "transport_fee":"0",
    "product_fee":"1",
    "discount":"0",
    "rmb_total_fee":""
  }
}
​``` * @param {Object} query query对象
```

方法签名

| 属性   | exports.orderQuery |
| ---- | ------------------ |
|      |                    |

#### api_poi: API

------

##### addPoi

创建门店 * Tips:

- 创建门店接口调用成功后不会实时返回poi_id。
- 成功创建后，门店信息会经过审核，审核通过后方可使用并获取poi_id。
- 图片photo_url必须为上传图片接口(api.uploadLogo，参见卡券接口)生成的url。
- 门店类目categories请参考微信公众号后台的门店管理部分。 * Poi:

```js
{
  "sid": "5794560",
  "business_name": "肯打鸡",
  "branch_name": "东方路店",
  "province": "上海市",
  "city": "上海市",
  "district": "浦东新区",
  "address": "东方路88号",
  "telephone": "021-5794560",
  "categories": ["美食,快餐小吃"],
  "offset_type": 1,
  "longitude": 125.5794560,
  "latitude": 45.5794560,
  "photo_list": [{
    "photo_url": "https://5794560.qq.com/1"
  }, {
    "photo_url": "https://5794560.qq.com/2"
  }],
  "recommend": "脉娜鸡腿堡套餐,脉乐鸡,全家捅",
  "special": "免费WIFE,外卖服务",
  "introduction": "肯打鸡是全球大型跨国连锁餐厅,2015年创立于米国,在世界上大约拥有3 亿间分店,主要售卖肯打鸡等垃圾食品",
  "open_time": "10:00-18:00",
  "avg_price": 88
}
```

Examples:

```js
api.addPoi(poi);
```

Result:

```js
{"errcode":0,"errmsg":"ok"}
```

方法签名

| 属性   | exports.addPoi |      |
| ---- | -------------- | ---- |
| 名字   | addPoi         |      |
| 参数   | poi(Object)    | 门店对象 |

##### getPoi

获取门店信息

Examples:

```js
api.getPoi(POI_ID);
```

Result:

```js
{
  "sid": "5794560",
  "business_name": "肯打鸡",
  "branch_name": "东方路店",
  "province": "上海市",
  "city": "上海市",
  "district": "浦东新区",
  "address": "东方路88号",
  "telephone": "021-5794560",
  "categories": ["美食,快餐小吃"],
  "offset_type": 1,
  "longitude": 125.5794560,
  "latitude": 45.5794560,
  "photo_list": [{
    "photo_url": "https://5794560.qq.com/1"
  }, {
    "photo_url": "https://5794560.qq.com/2"
  }],
  "recommend": "脉娜鸡腿堡套餐,脉乐鸡,全家捅",
  "special": "免费WIFE,外卖服务",
  "introduction": "肯打鸡是全球大型跨国连锁餐厅,2015年创立于米国,在世界上大约拥有3 亿间分店,主要售卖肯打鸡等垃圾食品",
  "open_time": "10:00-18:00",
  "avg_price": 88,
  "available_state": 3,
  "update_status": 0
}
```

方法签名

| 属性   | exports.getPoi |      |
| ---- | -------------- | ---- |
| 名字   | getPoi         |      |
| 参数   | poiId(Number)  | 门店ID |

##### getPois

获取门店列表
Examples:

```js
api.getPois(0, 20);
```

Result:

```js
{
  "errcode": 0,
  "errmsg": "ok"
  "business_list": [{
    "base_info": {
      "sid": "100",
      "poi_id": "5794560",
      "business_name": "肯打鸡",
      "branch_name": "东方路店",
      "address": "东方路88号",
      "available_state": 3
    }
  }, {
    "base_info": {
      "sid": "101",
      "business_name": "肯打鸡",
      "branch_name": "西方路店",
      "address": "西方路88号",
      "available_state": 4
    }
  }],
  "total_count": "2",
}
```

方法签名

| 属性   | exports.getPois |                     |
| ---- | --------------- | ------------------- |
| 名字   | getPois         |                     |
| 参数   | begin(Number)   | 开始位置，0即为从第一条开始查询    |
| 参数   | limit(Number)   | 返回数据条数，最大允许50，默认为20 |

##### delPoi

删除门店
Tips:

- 待审核门店不允许删除 * Examples:

```
api.delPoi(POI_ID);
```

方法签名

| 属性   | exports.delPoi |      |
| ---- | -------------- | ---- |
| 名字   | delPoi         |      |
| 参数   | poiId(Number)  | 门店ID |

##### updatePoi

修改门店服务信息 * Tips: * - 待审核门店不允许修改 * Poi:

```js
{
  "poi_id": "5794560",
  "telephone": "021-5794560",
  "photo_list": [{
    "photo_url": "https://5794560.qq.com/1"
  }, {
    "photo_url": "https://5794560.qq.com/2"
  }],
  "recommend": "脉娜鸡腿堡套餐,脉乐鸡,全家捅",
  "special": "免费WIFE,外卖服务",
  "introduction": "肯打鸡是全球大型跨国连锁餐厅,2015年创立于米国,在世界上大约拥有3 亿间分店,主要售卖肯打鸡等垃圾食品",
  "open_time": "10:00-18:00",
  "avg_price": 88
}
```

特别注意，以上7个字段，若有填写内容则为覆盖更新，若无内容则视为不修改，维持原有内容。
photo_list字段为全列表覆盖，若需要增加图片，需将之前图片同样放入list中，在其后增加新增图片。 * Examples:

```
api.updatePoi(poi);
```

Result:

```js
{"errcode":0,"errmsg":"ok"}
```

方法签名

| 属性   | exports.updatePoi |      |
| ---- | ----------------- | ---- |
| 名字   | updatePoi         |      |
| 参数   | poi(Object)       | 门店对象 |

#### api_qrcode: API

------

##### createTmpQRCode

创建临时二维码
详细请看：[http://mp.weixin.qq.com/wiki/index.php?title=生成带参数的二维码](http://mp.weixin.qq.com/wiki/index.php?title=%E7%94%9F%E6%88%90%E5%B8%A6%E5%8F%82%E6%95%B0%E7%9A%84%E4%BA%8C%E7%BB%B4%E7%A0%81)
Examples:

```js
api.createTmpQRCode(10000, 1800);
```

Result:

```js
{
 "ticket":"gQG28DoAAAAAAAAAASxodHRwOi8vd2VpeGluLnFxLmNvbS9xL0FuWC1DNmZuVEhvMVp4NDNMRnNRAAIEesLvUQMECAcAAA==",
 "expire_seconds":1800
}
```

方法签名

| 属性   | exports.createTmpQRCode |                    |
| ---- | ----------------------- | ------------------ |
| 参数   | sceneId(Number)         | 场景ID               |
| 参数   | expire(Number)          | 过期时间，单位秒。最大不超过1800 |

##### createLimitQRCode

创建永久二维码
详细请看：[http://mp.weixin.qq.com/wiki/index.php?title=生成带参数的二维码](http://mp.weixin.qq.com/wiki/index.php?title=%E7%94%9F%E6%88%90%E5%B8%A6%E5%8F%82%E6%95%B0%E7%9A%84%E4%BA%8C%E7%BB%B4%E7%A0%81)
Examples:

```js
api.createLimitQRCode(100);
```

Result:

```js
{
 "ticket":"gQG28DoAAAAAAAAAASxodHRwOi8vd2VpeGluLnFxLmNvbS9xL0FuWC1DNmZuVEhvMVp4NDNMRnNRAAIEesLvUQMECAcAAA=="
}
```

方法签名

| 属性   | exports.createLimitQRCode |                   |
| ---- | ------------------------- | ----------------- |
| 参数   | sceneId(Number)           | 场景ID。ID不能大于100000 |

##### showQRCodeURL

生成显示二维码的链接。微信扫描后，可立即进入场景
Examples:

```js
api.showQRCodeURL(ticket);
// => https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=TICKET
```

方法签名

| 方法   | exports.showQRCodeURL() |                           |
| ---- | ----------------------- | ------------------------- |
| 参数   | ticket(String)          | 二维码Ticket                 |
| 返回   | String                  | 显示二维码的URL地址，通过img标签可以显示出来 |

#### api_semantic: API

------

##### semantic

发送语义理解请求
详细请看：http://mp.weixin.qq.com/wiki/index.php?title=%E8%AF%AD%E4%B9%89%E7%90%86%E8%A7%A3 * Opts:

```js
{
  "query":"查一下明天从北京到上海的南航机票",
  "city":"北京",
  "category": "flight,hotel"
}
```

Examples:

```js
api.semantic(uid, opts);
```

Result:

```js
{
  "errcode":0,
  "query":"查一下明天从北京到上海的南航机票",
  "type":"flight",
  "semantic":{
      "details":{
          "start_loc":{
              "type":"LOC_CITY",
              "city":"北京市",
              "city_simple":"北京",
              "loc_ori":"北京"
              },
          "end_loc": {
              "type":"LOC_CITY",
              "city":"上海市",
              "city_simple":"上海",
              "loc_ori":"上海"
            },
          "start_date": {
              "type":"DT_ORI",
              "date":"2014-03-05",
              "date_ori":"明天"
            },
         "airline":"中国南方航空公司"
      },
  "intent":"SEARCH"
}
```

方法签名

| 属性   | exports.semantic |      |
| ---- | ---------------- | ---- |
| 参数   | openid(String)   | 用户ID |
| 参数   | opts(Object)     | 查询条件 |

#### api_shakearound: API

------

##### registerShakeAccount

申请开通功能
接口说明:
申请开通摇一摇周边功能。成功提交申请请求后，工作人员会在三个工作日内完成审核。若审核不通过，可以重新提交申请请求。
若是审核中，请耐心等待工作人员审核，在审核中状态不能再提交申请请求。
详情请参见：<http://mp.weixin.qq.com/wiki/13/025f1d471dc999928340161c631c6635.html> * Options:

```js
{
 "name": "zhang_san",
 "phone_number": "13512345678",
 "email": "weixin123@qq.com",
 "industry_id": "0118",
 "qualification_cert_urls": [
   "http://shp.qpic.cn/wx_shake_bus/0/1428565236d03d864b7f43db9ce34df5f720509d0e/0",
   "http://shp.qpic.cn/wx_shake_bus/0/1428565236d03d864b7f43db9ce34df5f720509d0e/0"
 ],
 "apply_reason": "test"
}
​``` * Examples:
```

api.registerShakeAccount(options);

Result:

```js
{
  "data" : { },
  "errcode": 0,
  "errmsg": "success."
}

```

方法签名

| 属性   | exports.registerShakeAccount |      |
| ---- | ---------------------------- | ---- |
| 参数   | options(Object)              | 请求参数 |

##### checkShakeAccountStatus

查询审核状态
接口说明：
查询已经提交的开通摇一摇周边功能申请的审核状态。在申请提交后，工作人员会在三个工作日内完成审核。
详情请参见：http://mp.weixin.qq.com/wiki/13/025f1d471dc999928340161c631c6635.html
Examples:

```
api.checkShakeAccountStatus();
```

Result:

```js
{
  "data" : {
    "apply_time": 1432026025,
    "audit_comment": "test",
    "audit_status": 1,       //审核状态。0：审核未通过、1：审核中、2：审核已通过；审核会在三个工作日内完成
    "audit_time": 0
  },
  "errcode": 0,
  "errmsg": "success."
}
​``` * @name checkShakeAccountStatus
```

方法签名

| 属性   | exports.checkShakeAccountStatus |
| ---- | ------------------------------- |
|      |                                 |

##### applyBeacons

设备管理: 申请设备ID。
接口说明:
申请配置设备所需的UUID、Major、Minor。若激活率小于50%，不能新增设备。单次新增设备超过500个，
需走人工审核流程。审核通过后，可用返回的批次ID用“查询设备列表”接口拉取本次申请的设备ID。
详情请参见：<http://mp.weixin.qq.com/wiki/15/b9e012f917e3484b7ed02771156411f3.html> * Options:

```js
{
  "quantity":3,
  "apply_reason":"测试",
  "comment":"测试专用",
  "poi_id":1234
}
​``` * Examples:
```

api.applyBeacons(options);

Result:

```js
{
  "data" : { ... },
  "errcode": 0,
  "errmsg": "success."
}

```

方法签名

| 属性   | exports.applyBeacons |      |
| ---- | -------------------- | ---- |
| 参数   | options(Object)      | 请求参数 |

##### updateBeacon

设备管理: 编辑设备的备注信息。
接口说明:
可用设备ID或完整的UUID、Major、Minor指定设备，二者选其一。
详情请参见：http://mp.weixin.qq.com/wiki/15/b9e012f917e3484b7ed02771156411f3.html
Options:

```js
{
 "device_identifier": {
   // 设备编号，若填了UUID、major、minor，则可不填设备编号，若二者都填，则以设备编号为优先
   "device_id": 10011,
   "uuid": "FDA50693-A4E2-4FB1-AFCF-C6EB07647825",
   "major": 1002,
   "minor": 1223
 },
 "comment": "test"
}
```

Examples:

```
api.updateBeacon(options);
```

Result:

```js
{
  "data" : {
  },
  "errcode": 0,
  "errmsg": "success."
}
​``` * @name updateBeacon
```

方法签名

| 属性   | exports.updateBeacon |      |
| ---- | -------------------- | ---- |
| 参数   | options(Object)      | 请求参数 |

##### bindBeaconLocation

设备管理: 配置设备与门店的关联关系。
接口说明:
修改设备关联的门店ID、设备的备注信息。可用设备ID或完整的UUID、Major、Minor指定设备，二者选其一。
详情请参见：http://mp.weixin.qq.com/wiki/15/b9e012f917e3484b7ed02771156411f3.html
Options:

```js
{
  "device_identifier": {
    "device_id": 10011,
    "uuid": "FDA50693-A4E2-4FB1-AFCF-C6EB07647825",
    "major": 1002,
    "minor": 1223
  },
  "poi_id": 1231
}
```

Examples:

```
api.bindBeaconLocation(options);
```

Result:

```js
{
  "data" : {
  },
  "errcode": 0,
  "errmsg": "success."
}
​``` * @name bindBeaconLocation
```

方法签名

| 属性   | exports.bindBeaconLocation |      |
| ---- | -------------------------- | ---- |
| 参数   | options(Object)            | 请求参数 |

##### getBeacons

设备管理: 查询设备列表
接口说明:
查询已有的设备ID、UUID、Major、Minor、激活状态、备注信息、关联门店、关联页面等信息。
可指定设备ID或完整的UUID、Major、Minor查询，也可批量拉取设备信息列表。
详情请参见：http://mp.weixin.qq.com/wiki/15/b9e012f917e3484b7ed02771156411f3.html
Options:

1. 查询指定设备时：

```js
{
 "device_identifier": [
   {
     "device_id":10011,
     "uuid":"FDA50693-A4E2-4FB1-AFCF-C6EB07647825",
     "major":1002,
     "minor":1223
   }
 ]
}
```

1. 需要分页查询或者指定范围内的设备时：

```js
{
  "begin": 0,
  "count": 3
}
```

1. 当需要根据批次ID查询时：

```js
{
  "apply_id": 1231,
  "begin": 0,
  "count": 3
}
```

Examples:

```
api.getBeacons(options);
```

Result:

```js
{
  "data": {
    "devices": [
      {
        "comment": "",
        "device_id": 10097,
        "major": 10001,
        "minor": 12102,
        "page_ids": "15369",
        "status": 1, //激活状态，0：未激活，1：已激活（但不活跃），2：活跃
        "poi_id": 0,
        "uuid": "FDA50693-A4E2-4FB1-AFCF-C6EB07647825"
      },
      {
        "comment": "",
        "device_id": 10098,
        "major": 10001,
        "minor": 12103,
        "page_ids": "15368",
        "status": 1,
        "poi_id": 0,
        "uuid": "FDA50693-A4E2-4FB1-AFCF-C6EB07647825"
      }
     ],
     "total_count": 151
   },
   "errcode": 0,
   "errmsg": "success."
}
​``` * @name getBeacons
```

方法签名

| 属性   | exports.getBeacons |      |
| ---- | ------------------ | ---- |
| 参数   | options(Object)    | 请求参数 |

##### createPage

页面管理: 新增页面
接口说明:
新增摇一摇出来的页面信息，包括在摇一摇页面出现的主标题、副标题、图片和点击进去的超链接。
其中，图片必须为用素材管理接口（uploadPageIcon函数）上传至微信侧服务器后返回的链接。
详情请参见：http://mp.weixin.qq.com/wiki/5/6626199ea8757c752046d8e46cf13251.html
Page:

```js
{
  "title":"主标题",
  "description":"副标题",
  "page_url":" https://zb.weixin.qq.com",
  "comment":"数据示例",
  "icon_url":"http://shp.qpic.cn/wx_shake_bus/0/14288351768a23d76e7636b56440172120529e8252/120"
  //调用uploadPageIcon函数获取到该URL
}
```

Examples:

```js
api.createPage(page);
```

Result:

```js
{
  "data" : {
    "page_id": 28840
  },
  "errcode": 0,
  "errmsg": "success."
}
​``` * @name createPage
```

方法签名

| 属性   | exports.createPage |      |
| ---- | ------------------ | ---- |
| 参数   | page(Object)       | 请求参数 |

##### updatePage

页面管理: 编辑页面信息
接口说明:
编辑摇一摇出来的页面信息，包括在摇一摇页面出现的主标题、副标题、图片和点击进去的超链接。
详情请参见：http://mp.weixin.qq.com/wiki/5/6626199ea8757c752046d8e46cf13251.html
Page:

```js
{
  "page_id":12306,
  "title":"主标题",
  "description":"副标题",
  "page_url":" https://zb.weixin.qq.com",
  "comment":"数据示例",
  "icon_url":"http://shp.qpic.cn/wx_shake_bus/0/14288351768a23d76e7636b56440172120529e8252/120"
  //调用uploadPageIcon函数获取到该URL
}
```

Examples:

```
api.updatePage(page);
```

Result:

```js
{
  "data" : {
    "page_id": 28840
  },
  "errcode": 0,
  "errmsg": "success."
}
​``` * @name updatePage
```

方法签名

| 属性   | exports.updatePage |      |
| ---- | ------------------ | ---- |
| 参数   | page(Object)       | 请求参数 |

##### deletePages

页面管理: 删除页面
接口说明:
删除已有的页面，包括在摇一摇页面出现的主标题、副标题、图片和点击进去的超链接。
只有页面与设备没有关联关系时，才可被删除。
详情请参见：http://mp.weixin.qq.com/wiki/5/6626199ea8757c752046d8e46cf13251.html
Page_ids:

```js
{
  "page_ids":[12345,23456,34567]
}
```

Examples:

```
api.deletePages(options);
```

Result:

```js
{
  "data" : {
  },
  "errcode": 0,
  "errmsg": "success."
}
```

方法签名

| 属性   | exports.deletePages |      |
| ---- | ------------------- | ---- |
| 名字   | deletePages         |      |
| 参数   | page_ids(Object)    | 请求参数 |

##### getPages

页面管理: 查询页面列表
接口说明:
查询已有的页面，包括在摇一摇页面出现的主标题、副标题、图片和点击进去的超链接。提供两种查询方式，可指定页面ID查询，也可批量拉取页面列表。
详情请参见：http://mp.weixin.qq.com/wiki/5/6626199ea8757c752046d8e46cf13251.html
Options:

1. 需要查询指定页面时：

```js
{
  "page_ids":[12345, 23456, 34567]
}
```

1. 需要分页查询或者指定范围内的页面时：

```js
{
  "begin": 0,
  "count": 3
}
​``` * Examples:
```

api.getBeacons(options);

Result:

```js
{
    "data":
    {
        "pages": [
            {
                "comment": "just for test",
                "description": "test",
                "icon_url": "https://www.baidu.com/img/bd_logo1",
                "page_id": 28840,
                "page_url": "http://xw.qq.com/testapi1",
                "title": "测试1"
            },
            {
                "comment": "just for test",
                "description": "test",
                "icon_url": "https://www.baidu.com/img/bd_logo1",
                "page_id": 28842,
                "page_url": "http://xw.qq.com/testapi2",
                "title": "测试2"
            }
        ],
        "total_count": 2
    },
    "errcode": 0,
    "errmsg": "success."
}
```

方法签名

| 属性   | exports.getPages |      |
| ---- | ---------------- | ---- |
| 参数   | options(Object)  | 请求参数 |

##### uploadPageIcon

上传图片素材
接口说明：
上传在摇一摇页面展示的图片素材，素材保存在微信侧服务器上。
格式限定为：jpg,jpeg,png,gif，图片大小建议120px*120 px，限制不超过200 px *200 px，图片需为正方形。
详情请参见：http://mp.weixin.qq.com/wiki/5/e997428269ff189d8f9a4b9e177be2d9.html
Examples:

```
api.uploadPageIcon('filepath');
```

Result:

```js
{
  "data" : {
    "pic_url": "http://shp.qpic.cn/wechat_shakearound_pic/0/1428377032e9dd2797018cad79186e03e8c5aec8dc/120"
  },
  "errcode": 0,
  "errmsg": "success."
}
​``` * @name uploadPageIcon
```

方法签名

| 属性   | exports.uploadPageIcon |      |
| ---- | ---------------------- | ---- |
| 参数   | filepath(String)       | 文件路径 |

##### bindBeaconWithPages

配置设备与页面的关联关系
接口说明:
配置设备与页面的关联关系。支持建立或解除关联关系，也支持新增页面或覆盖页面等操作。
配置完成后，在此设备的信号范围内，即可摇出关联的页面信息。若设备配置多个页面，则随机出现页面信息。
详情请参见：http://mp.weixin.qq.com/wiki/6/c449687e71510db19564f2d2d526b6ea.html
Options:

```js
{
 "device_identifier": {
   // 设备编号，若填了UUID、major、minor，则可不填设备编号，若二者都填，则以设备编号为优先
   "device_id":10011,
   "uuid":"FDA50693-A4E2-4FB1-AFCF-C6EB07647825",
   "major":1002,
   "minor":1223
 },
 "page_ids":[12345, 23456, 334567]
}
```

Examples:

```
api.bindBeaconWithPages(options);
```

Result:

```js
{
  "data" : {
  },
  "errcode": 0,
  "errmsg": "success."
}
​``` * @name bindBeaconWithPages
```

方法签名

| 属性   | exports.bindBeaconWithPages |      |
| ---- | --------------------------- | ---- |
| 参数   | options(Object)             | 请求参数 |

##### searchBeaconPageRelation

查询设备与页面的关联关系
接口说明:
查询设备与页面的关联关系。提供两种查询方式，可指定页面ID分页查询该页面所关联的所有的设备信息；
也可根据设备ID或完整的UUID、Major、Minor查询该设备所关联的所有页面信息。
详情请参见：http://mp.weixin.qq.com/wiki/6/c449687e71510db19564f2d2d526b6ea.html
Options:

1. 当查询指定设备所关联的页面时：

```js
{
 "type": 1,
 "device_identifier": {
   // 设备编号，若填了UUID、major、minor，则可不填设备编号，若二者都填，则以设备编号为优先
   "device_id":10011,
   "uuid":"FDA50693-A4E2-4FB1-AFCF-C6EB07647825",
   "major":1002,
   "minor":1223
 }
}
```


2.当查询页面所关联的设备时：

```js
{
  "type": 2,
  "page_id": 11101,
  "begin": 0,
  "count": 3
}
```

Examples:

```
api.searchBeaconPageRelation(options);
```

Result:

```js
{
 "data": {
   "relations": [
     {
       "device_id": 797994,
       "major": 10001,
       "minor": 10023,
       "page_id": 50054,
       "uuid": "FDA50693-A4E2-4FB1-AFCF-C6EB07647825"
     },
     {
       "device_id": 797994,
       "major": 10001,
       "minor": 10023,
       "page_id": 50055,
       "uuid": "FDA50693-A4E2-4FB1-AFCF-C6EB07647825"
     }
   ],
   "total_count": 2
 },
 "errcode": 0,
 "errmsg": "success."
}
​``` * @name searchBeaconPageRelation
```

方法签名

| 属性   | exports.searchBeaconPageRelation |      |
| ---- | -------------------------------- | ---- |
| 参数   | options(Object)                  | 请求参数 |

##### getShakeInfo

获取摇周边的设备及用户信息
接口说明:
获取设备信息，包括UUID、major、minor，以及距离、openID等信息。
详情请参见：http://mp.weixin.qq.com/wiki/3/34904a5db3d0ec7bb5306335b8da1faf.html
Ticket:

```js
{
  "ticket":”6ab3d8465166598a5f4e8c1b44f44645”
}
```

Examples:

```
api.getShakeInfo(ticket);
```

Result:

```js
{
  "data" : {
  },
  "errcode": 0,
  "errmsg": "success."
}
​``` * @name getShakeInfo
```

方法签名

| 属性   | exports.getShakeInfo |                                          |
| ---- | -------------------- | ---------------------------------------- |
| 参数   | ticket(Object)       | 摇周边业务的ticket，可在摇到的URL中得到，ticket生效时间为30分钟 |

##### getDeviceStatistics

数据统计: 以设备为维度的数据统计接口
接口说明:
查询单个设备进行摇周边操作的人数、次数，点击摇周边消息的人数、次数；查询的最长时间跨度为30天。
详情请参见：http://mp.weixin.qq.com/wiki/0/8a24bcacad40fe7ee98d1573cb8a6764.html
Options:

```js
{
  "device_identifier": {
    "device_id":10011,  //设备编号，若填了UUID、major、minor，则可不填设备编号，若二者都填，则以设备编号为优先
    "uuid":"FDA50693-A4E2-4FB1-AFCF-C6EB07647825", //UUID、major、minor，三个信息需填写完整，若填了设备编号，则可不填此信息。
    "major":1002,
    "minor":1223
  },
  "begin_date": 12313123311,
  "end_date": 123123131231
}
```

Examples:

```
api.getDeviceStatistics(options);
```

Result:

```js
{
  "data" : {
    {
      "click_pv": 0,
      "click_uv": 0,
      "ftime": 1425052800,
      "shake_pv": 0,
      "shake_uv": 0
    },
    {
      "click_pv": 0,
      "click_uv": 0,
      "ftime": 1425139200,
      "shake_pv": 0,
      "shake_uv": 0
    }
  },
  "errcode": 0,
  "errmsg": "success."
}
```

方法签名

| 属性   | exports.getDeviceStatistics |      |
| ---- | --------------------------- | ---- |
| 名字   | getDeviceStatistics         |      |
| 参数   | options(Object)             | 请求参数 |

##### getPageStatistics

数据统计: 以页面为维度的数据统计接口
接口说明:
查询单个页面通过摇周边摇出来的人数、次数，点击摇周边页面的人数、次数；查询的最长时间跨度为30天。
详情请参见：http://mp.weixin.qq.com/wiki/0/8a24bcacad40fe7ee98d1573cb8a6764.html
Options:

```js
{
  "page_id": 12345,
  "begin_date": 12313123311,
  "end_date": 123123131231
}
```

Examples:

```
api.getPageStatistics(options);
```

Result:

```js
{
  "data" : {
    {
      "click_pv": 0,
      "click_uv": 0,
      "ftime": 1425052800,
      "shake_pv": 0,
      "shake_uv": 0
    },
    {
      "click_pv": 0,
      "click_uv": 0,
      "ftime": 1425139200,
      "shake_pv": 0,
      "shake_uv": 0
    }
  },
  "errcode": 0,
  "errmsg": "success."
}
​``` * @name getPageStatistics
```

方法签名

| 属性   | exports.getPageStatistics |      |
| ---- | ------------------------- | ---- |
| 参数   | options(Object)           | 请求参数 |

#### api_shop_common: API

------

##### uploadPicture

上传图片
详细请看：[http://mp.weixin.qq.com/wiki/index.php?title=微信小店接口](http://mp.weixin.qq.com/wiki/index.php?title=%E5%BE%AE%E4%BF%A1%E5%B0%8F%E5%BA%97%E6%8E%A5%E5%8F%A3)
Examples:

```
api.uploadPicture('/path/to/your/img.jpg');
```

Result:

```js
{
 "errcode": 0,
 "errmsg": "success"
 "image_url": "http://mmbiz.qpic.cn/mmbiz/4whpV1VZl2ibl4JWwwnW3icSJGqecVtRiaPxwWEIr99eYYL6AAAp1YBo12CpQTXFH6InyQWXITLvU4CU7kic4PcoXA/0"
}
```

方法签名

| 属性   | exports.uploadPicture |      |
| ---- | --------------------- | ---- |
| 参数   | filepath(String)      | 文件路径 |

#### api_shop_express: API

------

##### addExpressTemplate

增加邮费模板
详细请看：[http://mp.weixin.qq.com/wiki/index.php?title=微信小店接口](http://mp.weixin.qq.com/wiki/index.php?title=%E5%BE%AE%E4%BF%A1%E5%B0%8F%E5%BA%97%E6%8E%A5%E5%8F%A3)
Examples:

```
api.addExpress(express);
```

Express:

```js
{
 "delivery_template": {
   "Name": "testexpress",
   "Assumer": 0,
   "Valuation": 0,
   "TopFee": [
     {
       "Type": 10000027,
       "Normal": {
         "StartStandards": 1,
         "StartFees": 2,
         "AddStandards": 3,
         "AddFees": 1
       },
       "Custom": [
         {
           "StartStandards": 1,
           "StartFees": 100,
           "AddStandards": 1,
           "AddFees": 3,
           "DestCountry": "中国",
           "DestProvince": "广东省",
           "DestCity": "广州市"
         }
       ]
     },
     {
       "Type": 10000028,
       "Normal": {
         "StartStandards": 1,
         "StartFees": 3,
         "AddStandards": 3,
         "AddFees": 2
       },
       "Custom": [
         {
           "StartStandards": 1,
           "StartFees": 10,
           "AddStandards": 1,
           "AddFees": 30,
           "DestCountry": "中国",
           "DestProvince": "广东省",
           "DestCity": "广州市"
         }
       ]
     },
     {
       "Type": 10000029,
       "Normal": {
         "StartStandards": 1,
         "StartFees": 4,
         "AddStandards": 3,
         "AddFees": 3
       },
       "Custom": [
         {
           "StartStandards": 1,
           "StartFees": 8,
           "AddStandards": 2,
           "AddFees": 11,
           "DestCountry": "中国",
           "DestProvince": "广东省",
           "DestCity": "广州市"
         }
       ]
     }
   ]
 }
}
```

Result:

```js
{
 "errcode": 0,
 "errmsg": "success"
 "template_id": 123456
}
```

方法签名

| 属性   | exports.addExpressTemplate |      |
| ---- | -------------------------- | ---- |
| 参数   | express(Object)            | 邮费模版 |

##### deleteExpressTemplate

修改邮费模板
详细请看：[http://mp.weixin.qq.com/wiki/index.php?title=微信小店接口](http://mp.weixin.qq.com/wiki/index.php?title=%E5%BE%AE%E4%BF%A1%E5%B0%8F%E5%BA%97%E6%8E%A5%E5%8F%A3)
Examples:

```
api.deleteExpressTemplate(templateId);
```

Result:

```js
{
 "errcode": 0,
 "errmsg": "success"
}
```

方法签名

| 属性   | exports.deleteExpressTemplate |        |
| ---- | ----------------------------- | ------ |
| 参数   | templateId(Number)            | 邮费模版ID |

##### updateExpressTemplate

修改邮费模板
详细请看：[http://mp.weixin.qq.com/wiki/index.php?title=微信小店接口](http://mp.weixin.qq.com/wiki/index.php?title=%E5%BE%AE%E4%BF%A1%E5%B0%8F%E5%BA%97%E6%8E%A5%E5%8F%A3)
Examples:

```
api.updateExpressTemplate(template);
```

Express:

```js
{
 "template_id": 123456,
 "delivery_template": ...
}
```

Result:

```js
{
 "errcode": 0,
 "errmsg": "success"
}
```

方法签名

| 属性   | exports.updateExpressTemplate |      |
| ---- | ----------------------------- | ---- |
| 参数   | template(Object)              | 邮费模版 |

##### getExpressTemplateById

获取指定ID的邮费模板
详细请看：[http://mp.weixin.qq.com/wiki/index.php?title=微信小店接口](http://mp.weixin.qq.com/wiki/index.php?title=%E5%BE%AE%E4%BF%A1%E5%B0%8F%E5%BA%97%E6%8E%A5%E5%8F%A3)
Examples:

```
api.getExpressTemplateById(templateId);
```

Result:

```js
{
 "errcode": 0,
 "errmsg": "success",
 "template_info": {
   "Id": 103312916,
   "Name": "testexpress",
   "Assumer": 0,
   "Valuation": 0,
   "TopFee": [
     {
       "Type": 10000027,
       "Normal": {
         "StartStandards": 1,
         "StartFees": 2,
         "AddStandards": 3,
         "AddFees": 1
       },
       "Custom": [
         {
           "StartStandards": 1,
           "StartFees": 1000,
           "AddStandards": 1,
           "AddFees": 3,
           "DestCountry": "中国",
           "DestProvince": "广东省",
           "DestCity": "广州市"
         }
       ]
     },
     ...
   ]
 }
}
```

方法签名

| 属性   | exports.getExpressTemplateById |        |
| ---- | ------------------------------ | ------ |
| 参数   | templateId(Number)             | 邮费模版Id |

##### getAllExpressTemplates

获取所有邮费模板的未封装版本
详细请看：[http://mp.weixin.qq.com/wiki/index.php?title=微信小店接口](http://mp.weixin.qq.com/wiki/index.php?title=%E5%BE%AE%E4%BF%A1%E5%B0%8F%E5%BA%97%E6%8E%A5%E5%8F%A3)
Examples:

```
api.getAllExpressTemplates();
```

Result:

```js
{
 "errcode": 0,
 "errmsg": "success",
 "templates_info": [
   {
     "Id": 103312916,
     "Name": "testexpress1",
     "Assumer": 0,
     "Valuation": 0,
     "TopFee": [...],
   },
   {
     "Id": 103312917,
     "Name": "testexpress2",
     "Assumer": 0,
     "Valuation": 2,
     "TopFee": [...],
   },
   {
     "Id": 103312918,
     "Name": "testexpress3",
     "Assumer": 0,
     "Valuation": 1,
     "TopFee": [...],
   }
 ]
}
```

方法签名

| 属性   | exports.getAllExpressTemplates |
| ---- | ------------------------------ |
|      |                                |

#### api_shop_goods: API

------

##### createGoods

增加商品
详细请看：[http://mp.weixin.qq.com/wiki/index.php?title=微信小店接口](http://mp.weixin.qq.com/wiki/index.php?title=%E5%BE%AE%E4%BF%A1%E5%B0%8F%E5%BA%97%E6%8E%A5%E5%8F%A3)
Examples:

```
api.createGoods(goods);
```

Goods:

```js
{
 "product_base":{
   "category_id":[
     "537074298"
   ],
   "property":[
     {"id":"1075741879","vid":"1079749967"},
     {"id":"1075754127","vid":"1079795198"},
     {"id":"1075777334","vid":"1079837440"}
   ],
   "name":"testaddproduct",
   "sku_info":[
     {
       "id":"1075741873",
       "vid":["1079742386","1079742363"]
     }
   ],
   "main_img": "http://mmbiz.qpic.cn/mmbiz/4whpV1VZl2iccsvYbHvnphkyGtnvjD3ulEKogfsiaua49pvLfUS8Ym0GSYjViaLic0FD3vN0V8PILcibEGb2fPfEOmw/0",
   "img":[
     "http://mmbiz.qpic.cn/mmbiz/4whpV1VZl2iccsvYbHvnphkyGtnvjD3ulEKogfsiaua49pvLfUS8Ym0GSYjViaLic0FD3vN0V8PILcibEGb2fPfEOmw/0"
   ],
   "detail":[
     {"text":"testfirst"},
     {"img": 4whpV1VZl2iccsvYbHvnphkyGtnvjD3ul1UcLcwxrFdwTKYhH9Q5YZoCfX4Ncx655ZK6ibnlibCCErbKQtReySaVA/0"},
     {"text":"testagain"}
   ],
   "buy_limit":10
 },
 "sku_list":[
   {
     "sku_id":"1075741873:1079742386",
     "price":30,
     "icon_url": "http://mmbiz.qpic.cn/mmbiz/4whpV1VZl28bJj62XgfHPibY3ORKicN1oJ4CcoIr4BMbfA8LqyyjzOZzqrOGz3f5K Wq1QGP3fo6TOTSYD3TBQjuw/0",
     "product_code":"testing",
     "ori_price":9000000,
     "quantity":800
   },
   {
     "sku_id":"1075741873:1079742363",
     "price":30,
     "icon_url": "http://mmbiz.qpic.cn/mmbiz/4whpV1VZl28bJj62XgfHPibY3ORKicN1oJ4CcoIr4BMbfA8LqyyjzOZzqrOGz3f5K Wq1QGP3fo6TOTSYD3TBQjuw/0",
     "product_code":"testingtesting",
     "ori_price":9000000,
     "quantity":800
   }
 ],
 "attrext":{
   "location":{
     "country":"中国",
     "province":"广东省",
     "city":"广州市",
     "address":"T.I.T创意园"
   },
   "isPostFree":0,
   "isHasReceipt":1,
   "isUnderGuaranty":0,
   "isSupportReplace":0
 },
 "delivery_info":{
   "delivery_type":0,
   "template_id":0,
   "express":[
     {"id":10000027,"price":100},
     {"id":10000028,"price":100},
     {"id":10000029,"price":100}
   ]
 }
}
```

Result:

```js
{
 "errcode": 0,
 "errmsg": "success",
 "product_id": "pDF3iYwktviE3BzU3BKiSWWi9Nkw"
}
```

方法签名

| 属性   | exports.createGoods |      |
| ---- | ------------------- | ---- |
| 参数   | goods(Object)       | 商品信息 |

##### deleteGoods

删除商品
详细请看：[http://mp.weixin.qq.com/wiki/index.php?title=微信小店接口](http://mp.weixin.qq.com/wiki/index.php?title=%E5%BE%AE%E4%BF%A1%E5%B0%8F%E5%BA%97%E6%8E%A5%E5%8F%A3)
Examples:

```
api.deleteGoods(productId);
```

Result:

```js
{
 "errcode": 0,
 "errmsg": "success",
}
```

方法签名

| 属性   | exports.deleteGoods |      |
| ---- | ------------------- | ---- |
| 参数   | productId(String)   | 商品Id |

##### updateGoods

修改商品
详细请看：[http://mp.weixin.qq.com/wiki/index.php?title=微信小店接口](http://mp.weixin.qq.com/wiki/index.php?title=%E5%BE%AE%E4%BF%A1%E5%B0%8F%E5%BA%97%E6%8E%A5%E5%8F%A3)
Examples:

```
api.updateGoods(goods);
```

Goods:

```js
{
 "product_id":"pDF3iY6Kr_BV_CXaiYysoGqJhppQ",
 "product_base":{
   "category_id":[
     "537074298"
   ],
   "property":[
     {"id":"1075741879","vid":"1079749967"},
     {"id":"1075754127","vid":"1079795198"},
     {"id":"1075777334","vid":"1079837440"}
   ],
   "name":"testaddproduct",
   "sku_info":[
     {
       "id":"1075741873",
       "vid":["1079742386","1079742363"]
     }
   ],
   "main_img": "http://mmbiz.qpic.cn/mmbiz/4whpV1VZl2iccsvYbHvnphkyGtnvjD3ulEKogfsiaua49pvLfUS8Ym0GSYjViaLic0FD3vN0V8PILcibEGb2fPfEOmw/0",
   "img":[
     "http://mmbiz.qpic.cn/mmbiz/4whpV1VZl2iccsvYbHvnphkyGtnvjD3ulEKogfsiaua49pvLfUS8Ym0GSYjViaLic0FD3vN0V8PILcibEGb2fPfEOmw/0"
   ],
   "detail":[
     {"text":"testfirst"},
     {"img": 4whpV1VZl2iccsvYbHvnphkyGtnvjD3ul1UcLcwxrFdwTKYhH9Q5YZoCfX4Ncx655ZK6ibnlibCCErbKQtReySaVA/0"},
     {"text":"testagain"}
   ],
   "buy_limit":10
 },
 "sku_list":[
   {
     "sku_id":"1075741873:1079742386",
     "price":30,
     "icon_url": "http://mmbiz.qpic.cn/mmbiz/4whpV1VZl28bJj62XgfHPibY3ORKicN1oJ4CcoIr4BMbfA8LqyyjzOZzqrOGz3f5K Wq1QGP3fo6TOTSYD3TBQjuw/0",
     "product_code":"testing",
     "ori_price":9000000,
     "quantity":800
   },
   {
     "sku_id":"1075741873:1079742363",
     "price":30,
     "icon_url": "http://mmbiz.qpic.cn/mmbiz/4whpV1VZl28bJj62XgfHPibY3ORKicN1oJ4CcoIr4BMbfA8LqyyjzOZzqrOGz3f5K Wq1QGP3fo6TOTSYD3TBQjuw/0",
     "product_code":"testingtesting",
     "ori_price":9000000,
     "quantity":800
   }
 ],
 "attrext":{
   "location":{
     "country":"中国",
     "province":"广东省",
     "city":"广州市",
     "address":"T.I.T创意园"
   },
   "isPostFree":0,
   "isHasReceipt":1,
   "isUnderGuaranty":0,
   "isSupportReplace":0
 },
 "delivery_info":{
   "delivery_type":0,
   "template_id":0,
   "express":[
     {"id":10000027,"price":100},
     {"id":10000028,"price":100},
     {"id":10000029,"price":100}
   ]
 }
}
```

Result:

```js
{
 "errcode": 0,
 "errmsg": "success"
}
```

方法签名

| 属性   | exports.updateGoods |      |
| ---- | ------------------- | ---- |
| 参数   | goods(Object)       | 商品信息 |

##### getGoods

查询商品
详细请看：[http://mp.weixin.qq.com/wiki/index.php?title=微信小店接口](http://mp.weixin.qq.com/wiki/index.php?title=%E5%BE%AE%E4%BF%A1%E5%B0%8F%E5%BA%97%E6%8E%A5%E5%8F%A3)
Examples:

```
api.getGoods(productId);
```

Result:

```js
{
 "errcode": 0,
 "errmsg": "success",
 "product_info":{
   "product_id":"pDF3iY6Kr_BV_CXaiYysoGqJhppQ",
   "product_base":{
     "name":"testaddproduct",
     "category_id":[537074298],
     "img":[
       "http://mmbiz.qpic.cn/mmbiz/4whpV1VZl2iccsvYbHvnphkyGtnvjD3ulEKogfsiaua49pvLfUS8Ym0GSYjViaLic0FD3vN0V8PILcibEGb2fPfEOmw/0"
     ],
     "property":[
       {"id":"品牌","vid":"Fujifilm/富⼠士"},
       {"id":"屏幕尺⼨寸","vid":"1.8英⼨寸"},
       {"id":"防抖性能","vid":"CCD防抖"}
     ],
     "sku_info":[
       {
         "id":"1075741873",
         "vid":[
           "1079742386",
           "1079742363"
         ]
       }
     ],
     "buy_limit":10,
     "main_img": 4whpV1VZl2iccsvYbHvnphkyGtnvjD3ulEKogfsiaua49pvLfUS8Ym0GSYjViaLic 0FD3vN0V8PILcibEGb2fPfEOmw/0",
     "detail_html": "<div class=\"item_pic_wrp\" style= \"margin-bottom:8px;font-size:0;\"><img class=\"item_pic\" style= \"width:100%;\" alt=\"\" src=\"http://mmbiz.qpic.cn/mmbiz/ 4whpV1VZl2iccsvYbHvnphkyGtnvjD3ulEKogfsiaua49pvLfUS8Ym0GSYjViaLic 0FD3vN0V8PILcibEGb2fPfEOmw/0\" ></div><p style=\"margin-bottom: 11px;margin-top:11px;\">test</p><div class=\"item_pic_wrp\" style=\"margin-bottom:8px;font-size:0;\"><img class=\"item_pic\" style=\"width:100%;\" alt=\"\" src=\"http://mmbiz.qpic.cn/mmbiz/ 4whpV1VZl2iccsvYbHvnphkyGtnvjD3ul1UcLcwxrFdwTKYhH9Q5YZoCfX4Ncx655 ZK6ibnlibCCErbKQtReySaVA/0\" ></div><p style=\"margin-bottom: 11px;margin-top:11px;\">test again</p>"
   },
   "sku_list":[
     {
       "sku_id":"1075741873:1079742386",
       "price":30,
       "icon_url": "http://mmbiz.qpic.cn/mmbiz/4whpV1VZl2iccsvYbHvnphkyGtnvjD3ulEKogfsiaua49pvLfUS8Ym0GSYjViaLic0FD3vN0V8PILcibEGb2fPfEOmw/0",
       "quantity":800,
       "product_code":"testing",
       "ori_price":9000000
     },
     {
       "sku_id":"1075741873:1079742363",
       "price":30,
       "icon_url": "http://mmbiz.qpic.cn/mmbiz/4whpV1VZl28bJj62XgfHPibY3ORKicN1oJ4CcoIr4BMbfA8LqyyjzOZzqrOGz3f5KWq1QGP3fo6TOTSYD3TBQjuw/0",
       "quantity":800,
       "product_code":"testingtesting",
       "ori_price":9000000
     }
   ],
   "attrext":{
     "isPostFree":0,
     "isHasReceipt":1,
     "isUnderGuaranty":0,
     "isSupportReplace":0,
     "location":{
       "country":"中国",
       "province":"广东省",
       "city":"⼲州市",
       "address":"T.I.T创意园"
     }
   },
   "delivery_info":{
     "delivery_type":1,
     "template_id":103312920
   }
 }
}
```

方法签名

| 属性   | exports.getGoods  |      |
| ---- | ----------------- | ---- |
| 参数   | productId(String) | 商品Id |

##### getGoodsByStatus

获取指定状态的所有商品
详细请看：[http://mp.weixin.qq.com/wiki/index.php?title=微信小店接口](http://mp.weixin.qq.com/wiki/index.php?title=%E5%BE%AE%E4%BF%A1%E5%B0%8F%E5%BA%97%E6%8E%A5%E5%8F%A3)
Examples:

```
api.deleteGoods(productId);
```

Result:

```js
{
 "errcode": 0,
 "errmsg": "success",
 "products_info": [
   {
     "product_base": ...,
     "sku_list": ...,
     "attrext": ...,
     "delivery_info": ...,
     "product_id": "pDF3iY-mql6CncpbVajaB_obC321",
     "status": 1
   }
 ]
}
```

方法签名

| 属性   | exports.getGoodsByStatus |                        |
| ---- | ------------------------ | ---------------------- |
| 参数   | status(Number)           | 状态码。(0-全部, 1-上架, 2-下架) |

##### updateGoodsStatus

商品上下架
详细请看：[http://mp.weixin.qq.com/wiki/index.php?title=微信小店接口](http://mp.weixin.qq.com/wiki/index.php?title=%E5%BE%AE%E4%BF%A1%E5%B0%8F%E5%BA%97%E6%8E%A5%E5%8F%A3)
Examples:

```
api.updateGoodsStatus(productId, status);
```

Result:

```js
{
 "errcode": 0,
 "errmsg": "success"
}
```

方法签名

| 属性   | exports.updateGoodsStatus |                        |
| ---- | ------------------------- | ---------------------- |
| 参数   | productId(String)         | 商品Id                   |
| 参数   | status(Number)            | 状态码。(0-全部, 1-上架, 2-下架) |

##### getSubCats

获取指定分类的所有子分类
详细请看：[http://mp.weixin.qq.com/wiki/index.php?title=微信小店接口](http://mp.weixin.qq.com/wiki/index.php?title=%E5%BE%AE%E4%BF%A1%E5%B0%8F%E5%BA%97%E6%8E%A5%E5%8F%A3)
Examples:

```
api.getSubCats(catId);
```

Result:

```js
{
 "errcode": 0,
 "errmsg": "success"
 "cate_list": [!
   {"id": "537074292","name": "数码相机"},
   {"id": "537074293","name": "家⽤用摄像机"},
   {"id": "537074298",! "name": "单反相机"}
 ]
}
```

方法签名

| 属性   | exports.getSubCats |       |
| ---- | ------------------ | ----- |
| 参数   | catId(Number)      | 大分类ID |

##### getSKUs

获取指定子分类的所有SKU
详细请看：[http://mp.weixin.qq.com/wiki/index.php?title=微信小店接口](http://mp.weixin.qq.com/wiki/index.php?title=%E5%BE%AE%E4%BF%A1%E5%B0%8F%E5%BA%97%E6%8E%A5%E5%8F%A3)
Examples:

```
api.getSKUs(catId);
```

Result:

```js
{
 "errcode": 0,
 "errmsg": "success"
 "sku_table": [
   {
     "id": "1075741873",
     "name": "颜⾊色",
     "value_list": [
       {"id": "1079742375", "name": "撞⾊色"},
       {"id": "1079742376","name": "桔⾊色"}
     ]
   }
 ]
}
```

方法签名

| 属性   | exports.getSKUs |       |
| ---- | --------------- | ----- |
| 参数   | catId(Number)   | 大分类ID |

##### getProperties

获取指定分类的所有属性
详细请看：[http://mp.weixin.qq.com/wiki/index.php?title=微信小店接口](http://mp.weixin.qq.com/wiki/index.php?title=%E5%BE%AE%E4%BF%A1%E5%B0%8F%E5%BA%97%E6%8E%A5%E5%8F%A3)
Examples:

```
api.getProperties(catId);
```

Result:

```js
{
 "errcode": 0,
 "errmsg": "success"
 "properties": [
   {
     "id": "1075741879",
     "name": "品牌",
     "property_value": [
       {"id": "200050867","name": "VIC&#38"},
       {"id": "200050868","name": "Kate&#38"},
       {"id": "200050971","name": "M&#38"},
       {"id": "200050972","name": "Black&#38"}
     ]
   },
   {
     "id": "123456789",
     "name": "颜⾊色",
     "property_value": ...
   }
 ]
}
```

方法签名

| 属性   | exports.getProperties |      |
| ---- | --------------------- | ---- |
| 参数   | catId(Number)         | 分类ID |

#### api_shop_group: API

------

##### createGoodsGroup

创建商品分组
详细请看：[http://mp.weixin.qq.com/wiki/index.php?title=微信小店接口](http://mp.weixin.qq.com/wiki/index.php?title=%E5%BE%AE%E4%BF%A1%E5%B0%8F%E5%BA%97%E6%8E%A5%E5%8F%A3)
Examples:

```
api.createGoodsGroup(groupName, productList);
```

Result:

```js
{
 "errcode": 0,
 "errmsg": "success",
 "group_id": 19
}
```

方法签名

| 属性   | exports.createGoodsGroup |        |
| ---- | ------------------------ | ------ |
| 参数   | groupName(String)        | 分组名    |
| 参数   | productList(Array)       | 该组商品列表 |
| 参数   | callback(Function)       | 回调函数   |

##### deleteGoodsGroup

删除商品分组
详细请看：[http://mp.weixin.qq.com/wiki/index.php?title=微信小店接口](http://mp.weixin.qq.com/wiki/index.php?title=%E5%BE%AE%E4%BF%A1%E5%B0%8F%E5%BA%97%E6%8E%A5%E5%8F%A3)
Examples:

```
api.deleteGoodsGroup(groupId);
```

Result:

```js
{
 "errcode": 0,
 "errmsg": "success"
}
```

方法签名

| 属性   | exports.deleteGoodsGroup |      |
| ---- | ------------------------ | ---- |
| 参数   | groupId(String)          | 分组ID |

##### updateGoodsGroup

修改商品分组属性
详细请看：[http://mp.weixin.qq.com/wiki/index.php?title=微信小店接口](http://mp.weixin.qq.com/wiki/index.php?title=%E5%BE%AE%E4%BF%A1%E5%B0%8F%E5%BA%97%E6%8E%A5%E5%8F%A3)
Examples:

```
api.updateGoodsGroup(groupId, groupName);
```

Result:

```js
{
 "errcode": 0,
 "errmsg": "success"
}
```

方法签名

| 属性   | exports.updateGoodsGroup |      |
| ---- | ------------------------ | ---- |
| 参数   | groupId(String)          | 分组ID |
| 参数   | groupName(String)        | 分组名  |

##### updateGoodsForGroup

修改商品分组内的商品
详细请看：[http://mp.weixin.qq.com/wiki/index.php?title=微信小店接口](http://mp.weixin.qq.com/wiki/index.php?title=%E5%BE%AE%E4%BF%A1%E5%B0%8F%E5%BA%97%E6%8E%A5%E5%8F%A3)
Examples:

```
api.updateGoodsForGroup(groupId, addProductList, delProductList);
```

Result:

```js
{
 "errcode": 0,
 "errmsg": "success"
}
```

方法签名

| 属性   | exports.updateGoodsForGroup |          |
| ---- | --------------------------- | -------- |
| 参数   | groupId(Object)             | 分组ID     |
| 参数   | addProductList(Array)       | 待添加的商品数组 |
| 参数   | delProductList(Array)       | 待删除的商品数组 |
| 参数   | callback(Function)          | 回调函数     |

##### getAllGroups

获取所有商品分组
详细请看：[http://mp.weixin.qq.com/wiki/index.php?title=微信小店接口](http://mp.weixin.qq.com/wiki/index.php?title=%E5%BE%AE%E4%BF%A1%E5%B0%8F%E5%BA%97%E6%8E%A5%E5%8F%A3)
Examples:

```
api.getAllGroups();
```

Result:

```js
{
 "errcode": 0,
 "errmsg": "success"
 "groups_detail": [
   {
     "group_id": 200077549,
     "group_name": "新品上架"
   },{
     "group_id": 200079772,
     "group_name": "全球热卖"
   }
 ]
}
```

方法签名

| 属性   | exports.getAllGroups |
| ---- | -------------------- |
|      |                      |

##### getGroupById

根据ID获取商品分组
详细请看：[http://mp.weixin.qq.com/wiki/index.php?title=微信小店接口](http://mp.weixin.qq.com/wiki/index.php?title=%E5%BE%AE%E4%BF%A1%E5%B0%8F%E5%BA%97%E6%8E%A5%E5%8F%A3)
Examples:

```
api.getGroupById(groupId);
```

Result:

```js
{
 "errcode": 0,
 "errmsg": "success"
 "group_detail": {
   "group_id": 200077549,
   "group_name": "新品上架",
   "product_list": [
     "pDF3iYzZoY-Budrzt8O6IxrwIJAA",
     "pDF3iY3pnWSGJcO2MpS2Nxy3HWx8",
     "pDF3iY33jNt0Dj3M3UqiGlUxGrio"
   ]
 }
}
```

方法签名

| 属性   | exports.getGroupById |      |
| ---- | -------------------- | ---- |
| 参数   | groupId(String)      | 分组ID |

#### api_shop_order: API

------

##### getOrderById

根据订单Id获取订单详情
详细请看：[http://mp.weixin.qq.com/wiki/index.php?title=微信小店接口](http://mp.weixin.qq.com/wiki/index.php?title=%E5%BE%AE%E4%BF%A1%E5%B0%8F%E5%BA%97%E6%8E%A5%E5%8F%A3)
Examples:

```
api.getOrderById(orderId);
```

Result:

```js
{
  "errcode": 0,
  "errmsg": "success",
  "order": {
    "order_id": "7197417460812533543",
    "order_status": 6,
    "order_total_price": 6,
    "order_create_time": 1394635817,
    "order_express_price": 5,
    "buyer_openid": "oDF3iY17NsDAW4UP2qzJXPsz1S9Q",
    "buyer_nick": "likeacat",
    "receiver_name": "张小猫",
    "receiver_province": "广东省",
    "receiver_city": "广州市",
    "receiver_address": "华景路一号南方通信大厦5楼",
    "receiver_mobile": "123456789",
    "receiver_phone": "123456789",
    "product_id": "pDF3iYx7KDQVGzB7kDg6Tge5OKFo",
    "product_name": "安莉芳E-BRA专柜女士舒适内衣蕾丝3/4薄杯聚拢上托性感文胸KB0716",
    "product_price": 1,
    "product_sku": "10000983:10000995;10001007:10001010",
    "product_count": 1,
    "product_img": "http://img2.paipaiimg.com/00000000/item-52B87243-63CCF66C00000000040100003565C1EA.0.300x300.jpg",
    "delivery_id": "1900659372473",
    "delivery_company": "059Yunda",
    "trans_id": "1900000109201404103172199813"
  }
}
```

方法签名

| 属性   | exports.getOrderById |      |
| ---- | -------------------- | ---- |
| 参数   | orderId(String)      | 订单Id |

##### getOrdersByStatus

根据订单状态/创建时间获取订单详情
详细请看：[http://mp.weixin.qq.com/wiki/index.php?title=微信小店接口](http://mp.weixin.qq.com/wiki/index.php?title=%E5%BE%AE%E4%BF%A1%E5%B0%8F%E5%BA%97%E6%8E%A5%E5%8F%A3)
Examples:

```
api.getOrdersByStatus([status,] [beginTime,] [endTime,]);
```

Usage:
当只传入callback参数时，查询所有状态，所有时间的订单
当传入一个参数，参数为Number类型，查询指定状态，所有时间的订单
当传入一个参数，参数为Date类型，查询所有状态，指定订单创建起始时间的订单(待测试)
当传入二个参数，第一参数为订单状态码，第二参数为订单创建起始时间
当传入三个参数，第一参数为订单状态码，第二参数为订单创建起始时间，第三参数为订单创建终止时间
Result:

```js
{
  "errcode": 0,
  "errmsg": "success",
  "order_list": [
    {
      "order_id": "7197417460812533543",
      "order_status": 6,
      "order_total_price": 6,
      "order_create_time": 1394635817,
      "order_express_price": 5,
      "buyer_openid": "oDF3iY17NsDAW4UP2qzJXPsz1S9Q",
      "buyer_nick": "likeacat",
      "receiver_name": "张小猫",
      "receiver_province": "广东省",
      "receiver_city": "广州市",
      "receiver_address": "华景路一号南方通信大厦5楼",
      "receiver_mobile": "123456",
      "receiver_phone": "123456",
      "product_id": "pDF3iYx7KDQVGzB7kDg6Tge5OKFo",
      "product_name": "安莉芳E-BRA专柜女士舒适内衣蕾丝3/4薄杯聚拢上托性感文胸KB0716",
      "product_price": 1,
      "product_sku": "10000983:10000995;10001007:10001010",
      "product_count": 1,
      "product_img": "http://mmbiz.qpic.cn/mmbiz/4whpV1VZl2icND8WwMThBEcehjhDv2icY4GrDSG5RLM3B2qd9kOicWGVJcsAhvXfibhWRNoGOvCfMC33G9z5yQr2Qw/0",
      "delivery_id": "1900659372473",
      "delivery_company": "059Yunda",
      "trans_id": "1900000109201404103172199813"
    },
    {
      "order_id": "7197417460812533569",
      "order_status": 8,
      "order_total_price": 1,
      "order_create_time": 1394636235,
      "order_express_price": 0,
      "buyer_openid": "oDF3iY17NsDAW4UP2qzJXPsz1S9Q",
      "buyer_nick": "likeacat",
      "receiver_name": "张小猫",
      "receiver_province": "广东省",
      "receiver_city": "广州市",
      "receiver_address": "华景路一号南方通信大厦5楼",
      "receiver_mobile": "123456",
      "receiver_phone": "123456",
      "product_id": "pDF3iYx7KDQVGzB7kDg6Tge5OKFo",
      "product_name": "项坠333",
      "product_price": 1,
      "product_sku": "1075741873:1079742377",
      "product_count": 1,
      "product_img": "http://mmbiz.qpic.cn/mmbiz/4whpV1VZl2icND8WwMThBEcehjhDv2icY4GrDSG5RLM3B2qd9kOicWGVJcsAhvXfibhWRNoGOvCfMC33G9z5yQr2Qw/0",
      "delivery_id": "1900659372473",
      "delivery_company": "059Yunda",
      "trans_id": "1900000109201404103172199813"
    }
  ]
}
```

方法签名

| 属性   | exports.getOrdersByStatus |                                          |
| ---- | ------------------------- | ---------------------------------------- |
| 参数   | status(Number)            | 状态码。(无此参数-全部状态, 2-待发货, 3-已发货, 5-已完成, 8-维权中) |
| 参数   | beginTime(Date)           | 订单创建时间起始时间。(无此参数则不按照时间做筛选)               |
| 参数   | endTime(Date)             | 订单创建时间终止时间。(无此参数则不按照时间做筛选)               |

##### setExpressForOrder

设置订单发货信息
详细请看：[http://mp.weixin.qq.com/wiki/index.php?title=微信小店接口](http://mp.weixin.qq.com/wiki/index.php?title=%E5%BE%AE%E4%BF%A1%E5%B0%8F%E5%BA%97%E6%8E%A5%E5%8F%A3)
Examples:

```
api.setExpressForOrder(orderId, deliveryCompany, deliveryTrackNo, isOthers);
```

Result:

```js
{
 "errcode": 0,
 "errmsg": "success"
}
```

方法签名

| 属性   | exports.setExpressForOrder |                                          |
| ---- | -------------------------- | ---------------------------------------- |
| 参数   | orderId(String)            | 订单Id                                     |
| 参数   | deliveryCompany(String)    | 物流公司 (物流公司Id请参考微信小店API手册)                |
| 参数   | deliveryTrackNo(String)    | 运单Id                                     |
| 参数   | isOthers(Number)           | 是否为6.4.5表之外的其它物流公司(0-否，1-是，无该字段默认为不是其它物流公司) |

##### setNoDeliveryForOrder

设置订单发货信息－不需要物流配送
适用于不需要实体物流配送的虚拟商品，完成本操作后订单即完成。
详细请看：[http://mp.weixin.qq.com/wiki/index.php?title=微信小店接口](http://mp.weixin.qq.com/wiki/index.php?title=%E5%BE%AE%E4%BF%A1%E5%B0%8F%E5%BA%97%E6%8E%A5%E5%8F%A3)
Examples:

```
api.setNoDeliveryForOrder(orderId);
```

Result:

```js
{
 "errcode": 0,
 "errmsg": "success"
}
```

方法签名

| 属性   | exports.setNoDeliveryForOrder |      |
| ---- | ----------------------------- | ---- |
| 参数   | orderId(String)               | 订单Id |

##### closeOrder

关闭订单
详细请看：[http://mp.weixin.qq.com/wiki/index.php?title=微信小店接口](http://mp.weixin.qq.com/wiki/index.php?title=%E5%BE%AE%E4%BF%A1%E5%B0%8F%E5%BA%97%E6%8E%A5%E5%8F%A3)
Examples:

```
api.closeOrder(orderId);
```

Result:

```js
{
 "errcode": 0,
 "errmsg": "success"
}
```

方法签名

| 属性   | exports.closeOrder |      |
| ---- | ------------------ | ---- |
| 参数   | orderId(String)    | 订单Id |

#### api_shop_shelf: API

------

##### createShelf

增加货架
详细请看：[http://mp.weixin.qq.com/wiki/index.php?title=微信小店接口](http://mp.weixin.qq.com/wiki/index.php?title=%E5%BE%AE%E4%BF%A1%E5%B0%8F%E5%BA%97%E6%8E%A5%E5%8F%A3)
Examples:

```
api.createShelf(shelf);
```

Shelf:

```js
{
  "shelf_data": {
    "module_infos": [
    {
      "group_info": {
        "filter": {
          "count": 2
        },
        "group_id": 50
      },
      "eid": 1
    },
    {
      "group_infos": {
        "groups": [
          {
            "group_id": 49
          },
          {
            "group_id": 50
          },
          {
            "group_id": 51
          }
        ]
      },
      "eid": 2
    },
    {
      "group_info": {
        "group_id": 52,
        "img": "http://mmbiz.qpic.cn/mmbiz/4whpV1VZl29nqqObBwFwnIX3licVPnFV5Jm64z4I0TTicv0TjN7Vl9bykUUibYKIOjicAwIt6Oy0Y6a1Rjp5Tos8tg/0"
      },
      "eid": 3
    },
    {
      "group_infos": {
        "groups": [
          {
            "group_id": 49,
            "img": "http://mmbiz.qpic.cn/mmbiz/4whpV1VZl29nqqObBwFwnIX3licVPnFV5uUQx7TLx4tB9qZfbe3JmqR4NkkEmpb5LUWoXF1ek9nga0IkeSSFZ8g/0"
          },
          {
            "group_id": 50,
            "img": "http://mmbiz.qpic.cn/mmbiz/4whpV1VZl29nqqObBwFwnIX3licVPnFV5G1kdy3ViblHrR54gbCmbiaMnl5HpLGm5JFeENyO9FEZAy6mPypEpLibLA/0"
          },
          {
            "group_id": 52,
            "img": "http://mmbiz.qpic.cn/mmbiz/4whpV1VZl29nqqObBwFwnIX3licVPnFV5uUQx7TLx4tB9qZfbe3JmqR4NkkEmpb5LUWoXF1ek9nga0IkeSSFZ8g/0"
          }
        ]
      },
      "eid": 4
    },
    {
      "group_infos": {
        "groups": [
          {
            "group_id": 43
          },
          {
            "group_id": 44
          },
          {
            "group_id": 45
          },
          {
            "group_id": 46
          }
        ],
      "img_background": "http://mmbiz.qpic.cn/mmbiz/4whpV1VZl29nqqObBwFwnIX3licVPnFV5uUQx7TLx4tB9qZfbe3JmqR4NkkEmpb5LUWoXF1ek9nga0IkeSSFZ8g/0"
      },
      "eid": 5
    }
    ]
  },
  "shelf_banner": "http://mmbiz.qpic.cn/mmbiz/4whpV1VZl2ibrWQn8zWFUh1YznsMV0XEiavFfLzDWYyvQOBBszXlMaiabGWzz5B2KhNn2IDemHa3iarmCyribYlZYyw/0",
  "shelf_name": "测试货架"
}
```

Result:

```js
{
  "errcode": 0,
  "errmsg": "success",
  "shelf_id": 12
}
```

方法签名

| 属性   | exports.createShelf |      |
| ---- | ------------------- | ---- |
| 参数   | shelf(Object)       | 货架信息 |

##### deleteShelf

删除货架
详细请看：[http://mp.weixin.qq.com/wiki/index.php?title=微信小店接口](http://mp.weixin.qq.com/wiki/index.php?title=%E5%BE%AE%E4%BF%A1%E5%B0%8F%E5%BA%97%E6%8E%A5%E5%8F%A3)
Examples:

```
api.deleteShelf(shelfId);
```

Result:

```js
{
  "errcode": 0,
  "errmsg": "success"
}
```

方法签名

| 属性   | exports.deleteShelf |      |
| ---- | ------------------- | ---- |
| 参数   | shelfId(String)     | 货架Id |

##### updateShelf

修改货架
详细请看：[http://mp.weixin.qq.com/wiki/index.php?title=微信小店接口](http://mp.weixin.qq.com/wiki/index.php?title=%E5%BE%AE%E4%BF%A1%E5%B0%8F%E5%BA%97%E6%8E%A5%E5%8F%A3)
Examples:

```
api.updateShelf(shelf);
```

Shelf:

```js
{
  "shelf_id": 12345,
  "shelf_data": ...,
  "shelf_banner": "http://mmbiz.qpic.cn/mmbiz/ 4whpV1VZl2ibrWQn8zWFUh1YznsMV0XEiavFfLzDWYyvQOBBszXlMaiabGWzz5B2K hNn2IDemHa3iarmCyribYlZYyw/0",
  "shelf_name": "货架名称"
}
```

Result:

```js
{
  "errcode": 0,
  "errmsg": "success"
}
```

方法签名

| 属性   | exports.updateShelf |      |
| ---- | ------------------- | ---- |
| 参数   | shelf(Object)       | 货架信息 |

##### getAllShelves

获取所有货架
详细请看：[http://mp.weixin.qq.com/wiki/index.php?title=微信小店接口](http://mp.weixin.qq.com/wiki/index.php?title=%E5%BE%AE%E4%BF%A1%E5%B0%8F%E5%BA%97%E6%8E%A5%E5%8F%A3)
Examples:

```
api.getAllShelf();
```

Result:

```js
{
  "errcode": 0,
  "errmsg": "success",
  "shelves": [
    {
      "shelf_info": {
      "module_infos": [
        {
        "group_infos": {
          "groups": [
          {
            "group_id": 200080093
          },
          {
            "group_id": 200080118
          },
          {
            "group_id": 200080119
          },
          {
            "group_id": 200080135
          }
          ],
          "img_background": "http://mmbiz.qpic.cn/mmbiz/4whpV1VZl294FzPwnf9dAcaN7ButStztAZyy2yHY8pW6sTQKicIhAy5F0a2CqmrvDBjMFLtc2aEhAQ7uHsPow9A/0"
        },
        "eid": 5
        }
      ]
      },
      "shelf_banner": "http://mmbiz.qpic.cn/mmbiz/4whpV1VZl294FzPwnf9dAcaN7ButStztAZyy2yHY8pW6sTQKicIhAy5F0a2CqmrvDBjMFLtc2aEhAQ7uHsPow9A/0",
      "shelf_name": "新新人类",
      "shelf_id": 22
    },
    {
      "shelf_info": {
      "module_infos": [
        {
          "group_info": {
            "group_id": 200080119,
            "filter": {
              "count": 4
            }
          },
          "eid": 1
        }
      ]
      },
      "shelf_banner": "http://mmbiz.qpic.cn/mmbiz/4whpV1VZl294FzPwnf9dAcaN7ButStztAZyy2yHY8pW6sTQKicIhAy5F0a2CqmrvDBjMFLtc2aEhAQ7uHsPow9A/0",
      "shelf_name": "店铺",
      "shelf_id": 23
    }
  ]
}
```

方法签名

| 属性   | exports.getAllShelves |
| ---- | --------------------- |
|      |                       |

##### getShelfById

根据货架ID获取货架信息
详细请看：[http://mp.weixin.qq.com/wiki/index.php?title=微信小店接口](http://mp.weixin.qq.com/wiki/index.php?title=%E5%BE%AE%E4%BF%A1%E5%B0%8F%E5%BA%97%E6%8E%A5%E5%8F%A3)
Examples:

```
api.getShelfById(shelfId);
```

Result:

```js
{
  "errcode": 0,
  "errmsg": "success",
  "shelf_info": {
    "module_infos": [...]
  },
  "shelf_banner": "http://mmbiz.qpic.cn/mmbiz/4whpV1VZl2ibp2DgDXiaic6WdflMpNdInS8qUia2BztlPu1gPlCDLZXEjia2qBdjoLiaCGUno9zbs1UyoqnaTJJGeEew/0",
  "shelf_name": "新建货架",
  "shelf_id": 97
}
```

方法签名

| 属性   | exports.getShelfById |      |
| ---- | -------------------- | ---- |
| 参数   | shelfId(String)      | 货架Id |

#### api_shop_stock: API

------

##### updateStock

增加库存
详细请看：[http://mp.weixin.qq.com/wiki/index.php?title=微信小店接口](http://mp.weixin.qq.com/wiki/index.php?title=%E5%BE%AE%E4%BF%A1%E5%B0%8F%E5%BA%97%E6%8E%A5%E5%8F%A3)
Examples:

```
api.updateStock(10, productId, sku); // 增加10件库存
api.updateStock(-10, productId, sku); // 减少10件库存
```

Result:

```js
{
 "errcode": 0,
 "errmsg": "success"
}
```

方法签名

| 属性   | exports.updateStock |           |
| ---- | ------------------- | --------- |
| 参数   | number(Number)      | 增加或者删除的数量 |
| 参数   | productId(String)   | 商品ID      |
| 参数   | sku(String)         | SKU信息     |

#### api_template: API

------

##### setIndustry

设置所属行业
Examples:

```js
const industryIds = {
 "industry_id1":'1',
 "industry_id2":"4"
};
api.setIndustry(industryIds);
```

方法签名

| 属性   | exports.setIndustry |               |
| ---- | ------------------- | ------------- |
| 参数   | industryIds(Object) | 公众号模板消息所属行业编号 |

##### addTemplate

获得模板ID
Examples:

```js
const templateIdShort = 'TM00015';
api.addTemplate(templateIdShort);
```

方法签名

| 属性   | exports.addTemplate     |                                   |
| ---- | ----------------------- | --------------------------------- |
| 参数   | templateIdShort(String) | 模板库中模板的编号，有“TM**”和“OPENTMTM**”等形式 |

##### sendTemplate

发送模板消息
Examples:

```js
const templateId: '模板id';
// URL置空，则在发送后,点击模板消息会进入一个空白页面（ios）, 或无法点击（android）
const url: 'http://weixin.qq.com/download';
const topcolor = '#FF0000'; // 顶部颜色
const data = {
 user:{
   "value":'黄先生',
   "color":"#173177"
 }
};
api.sendTemplate('openid', templateId, url, topColor, data);
```

方法签名

| 属性   | exports.sendTemplate |                                          |
| ---- | -------------------- | ---------------------------------------- |
| 参数   | openid(String)       | 用户的openid                                |
| 参数   | templateId(String)   | 模板ID                                     |
| 参数   | url(String)          | URL置空，则在发送后，点击模板消息会进入一个空白页面（ios），或无法点击（android） |
| 参数   | topColor(String)     | 顶部颜色                                     |
| 参数   | data(Object)         | 渲染模板的数据                                  |

#### api_url: API

------

##### shorturl

短网址服务
详细细节 http://mp.weixin.qq.com/wiki/index.php?title=长链接转短链接接口
Examples:

```
api.shorturl('http://mp.weixin.com');
```

方法签名

| 属性   | exports.shorturl |                                          |
| ---- | ---------------- | ---------------------------------------- |
| 参数   | longUrl(String)  | 需要转换的长链接，支持http://、https://、weixin://wxpay格式的url |

#### api_user: API

------

##### getUser

获取用户基本信息。可以设置lang，其中zh_CN 简体，zh_TW 繁体，en 英语。默认为en
详情请见：[http://mp.weixin.qq.com/wiki/index.php?title=获取用户基本信息](http://mp.weixin.qq.com/wiki/index.php?title=%E8%8E%B7%E5%8F%96%E7%94%A8%E6%88%B7%E5%9F%BA%E6%9C%AC%E4%BF%A1%E6%81%AF)
Examples:

```js
api.getUser(openid);
api.getUser({openid: 'openid', lang: 'en'});
```

Result:

```js
{
 "subscribe": 1,
 "openid": "o6_bmjrPTlm6_2sgVt7hMZOPfL2M",
 "nickname": "Band",
 "sex": 1,
 "language": "zh_CN",
 "city": "广州",
 "province": "广东",
 "country": "中国",
 "headimgurl": "http://wx.qlogo.cn/mmopen/g3MonUZtNHkdmzicIlibx6iaFqAc56vxLSUfpb6n5WKSYVY0ChQKkiaJSgQ1dZuTOgvLLrhJbERQQ4eMsv84eavHiaiceqxibJxCfHe/0",
 "subscribe_time": 1382694957
}
```

方法签名

| 属性   | exports.getUser        |                                     |
| ---- | ---------------------- | ----------------------------------- |
| 参数   | options(String,Object) | 用户的openid。或者配置选项，包含openid和lang两个属性。 |

##### batchGetUsers

批量获取用户基本信息
Example:

```
api.batchGetUsers(['openid1', 'openid2'])
```

Result:

```js
{
  "user_info_list": [{
    "subscribe": 1,
    "openid": "otvxTs4dckWG7imySrJd6jSi0CWE",
    "nickname": "iWithery",
    "sex": 1,
    "language": "zh_CN",
    "city": "Jieyang",
    "province": "Guangdong",
    "country": "China",
    "headimgurl": "http://wx.qlogo.cn/mmopen/xbIQx1GRqdvyqkMMhEaGOX802l1CyqMJNgUzKP8MeAeHFicRDSnZH7FY4XB7p8XHXIf6uJA2SCunTPicGKezDC4saKISzRj3nz/0",
    "subscribe_time": 1434093047,
    "unionid": "oR5GjjgEhCMJFyzaVZdrxZ2zRRF4",
    "remark": "",
    "groupid": 0
  }, {
    "subscribe": 0,
    "openid": "otvxTs_JZ6SEiP0imdhpi50fuSZg",
    "unionid": "oR5GjjjrbqBZbrnPwwmSxFukE41U",
  }]
}
```

方法签名

| 属性   | exports.batchGetUsers |              |
| ---- | --------------------- | ------------ |
| 参数   | openids(Array)        | 用户的openid数组。 |

##### getFollowers

获取关注者列表
详细细节 http://mp.weixin.qq.com/wiki/index.php?title=获取关注者列表
Examples:

```js
api.getFollowers();
// or
api.getFollowers(nextOpenid);
```

Result:

```js
{
 "total":2,
 "count":2,
 "data":{
   "openid":["","OPENID1","OPENID2"]
 },
 "next_openid":"NEXT_OPENID"
}
```

方法签名

| 属性   | exports.getFollowers |                                  |
| ---- | -------------------- | -------------------------------- |
| 参数   | nextOpenid(String)   | 调用一次之后，传递回来的nextOpenid。第一次获取时可不填 |

##### updateRemark

设置用户备注名
详细细节 http://mp.weixin.qq.com/wiki/index.php?title=设置用户备注名接口
Examples:

```
api.updateRemark(openid, remark);
```

Result:

```js
{
 "errcode":0,
 "errmsg":"ok"
}
```

方法签名

| 属性   | exports.updateRemark |                  |
| ---- | -------------------- | ---------------- |
| 参数   | openid(String)       | 用户的openid        |
| 参数   | remark(String)       | 新的备注名，长度必须小于30字符 |

##### createTags

创建标签
详细细节 https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421140837&token=&lang=zh_CN
Examples:

```
api.createTags(name);
```

Result:

```js
{
 "id":tagId,
 "name":tagName
}
```

方法签名

| 属性   | exports.createTags |      |
| ---- | ------------------ | ---- |
| 参数   | name(String)       | 标签名  |

##### getTags

获取公众号已创建的标签
详细细节 https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421140837&token=&lang=zh_CN
Examples:

```
api.getTags();
```

Result:

```js
 {
   "tags":[{
       "id":1,
       "name":"每天一罐可乐星人",
       "count":0 //此标签下粉丝数
 },{
   "id":2,
   "name":"星标组",
   "count":0
 },{
   "id":127,
   "name":"广东",
   "count":5
 }
   ]
 }
```

方法签名

| 属性   | exports.getTags |
| ---- | --------------- |
|      |                 |

##### updateTag

编辑标签
详细细节 https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421140837&token=&lang=zh_CN
Examples:

```
api.updateTag(id,name);
```

Result:

```js
 {
   "errcode":0,
   "errmsg":"ok"
 }
```

方法签名

| 属性   | exports.updateTag |      |
| ---- | ----------------- | ---- |
| 参数   | id(String)        | 标签id |
| 参数   | name(String)      | 标签名  |

##### deleteTag

删除标签
详细细节 https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421140837&token=&lang=zh_CN
Examples:

```
api.deleteTag(id);
```

Result:

```js
 {
   "errcode":0,
   "errmsg":"ok"
 }
```

方法签名

| 属性   | exports.deleteTag |      |
| ---- | ----------------- | ---- |
| 参数   | id(String)        | 标签id |

##### getUsersFromTag

获取标签下粉丝列表
详细细节 https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421140837&token=&lang=zh_CN
Examples:

```
api.getUsersFromTag(tagId,nextOpenId);
```

Result:

```js
 {
 "count":2,//这次获取的粉丝数量
 "data":{//粉丝列表
   "openid":[
      "ocYxcuAEy30bX0NXmGn4ypqx3tI0",
      "ocYxcuBt0mRugKZ7tGAHPnUaOW7Y"
    ]
  },
```

方法签名

| 属性   | exports.getUsersFromTag |                         |
| ---- | ----------------------- | ----------------------- |
| 参数   | tagId(String)           | 标签id                    |
| 参数   | nextOpenId(String)      | 第一个拉取的OPENID，不填默认从头开始拉取 |

##### batchTagging

批量为用户打标签
详细细节 https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421140837&token=&lang=zh_CN
Examples:

```
api.batchTagging(openIdList,tagId);
```

Result:

```js
 {
   "errcode":0,
   "errmsg":"ok"
 }
```

方法签名

| 属性   | exports.batchTagging |          |
| ---- | -------------------- | -------- |
| 参数   | openIdList(Array)    | openId列表 |
| 参数   | tagId(String)        | 标签id     |

##### batchUnTagging

批量为用户取消标签
详细细节 https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421140837&token=&lang=zh_CN
Examples:

```
api.batchUnTagging(openIdList,tagId);
```

Result:

```js
 {
   "errcode":0,
   "errmsg":"ok"
 }
```

方法签名

| 属性   | exports.batchUnTagging |          |
| ---- | ---------------------- | -------- |
| 参数   | openIdList(Array)      | openId列表 |
| 参数   | tagId(String)          | 标签id     |

##### getIdList

获取用户身上的标签列表
详细细节 https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421140837&token=&lang=zh_CN
Examples:

```
api.getIdList(openId);
```

Result:

```js
 {
   "tagid_list":[//被置上的标签列表 134,2]
  }
```

方法签名

| 属性   | exports.getIdList |
| ---- | ----------------- |
|      |                   |