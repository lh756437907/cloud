const router = require("koa-router")();
const Joi = require("joi");
const { Payment, Notification } = require("easy-alipay");
const defaultConfig = require("easy-alipay/lib/default-config");
const alipaySubmit = require("easy-alipay/lib/alipay-submit");

/**
 *
 * api {get} /home/app/plugins/payAlipay/config 获取支付宝支付配置
 *
 */
router.get("/payAlipay/config", async (ctx, nex) => {
  const baas = ctx.baas;
  const config = await BaaS.Models.pay_alipay
    .query({ where: { baas_id: baas.id } })
    .fetch({ withRelated: ["class", "function"] });
  ctx.success(config);
});
/**
 *
 * api {get} /home/app/plugins/payAlipay/config 提交支付宝支付配置
 *
 */
router.post("/payAlipay/config", async (ctx, nex) => {
  const baas = ctx.baas;
  const { id, account = "", partner = "", key = "" } = ctx.post;
  // 验证表单是否为空
  const isEmpty = ctx.isEmpty(
    {
      account: account,
      partner: partner,
      key: key
    },
    ["account", "partner", "key"]
  );
  if (!isEmpty) {
    ctx.error("请完善表单");
    return;
  }
  // 查询是否已配置过
  const wxConfig = await BaaS.Models.pay_alipay
    .query({
      where: { baas_id: baas.id }
    })
    .fetch();
  const dataAdd = {
    baas_id: baas.id,
    account: account,
    partner: partner,
    key: key
  };
  Object.assign(dataAdd, { id: wxConfig.id });
  const config = await BaaS.Models.pay_alipay.forge(dataAdd).save();
  ctx.success(config);
});
/**
 *
 * api {get} /home/app/plugins/payAlipay/config 提交支付宝支付回调
 *
 */
router.post("/payAlipay/payCall", async (ctx, next) => {
  const baas = ctx.baas;
  const classId = ctx.post.class_id;
  const functionId = ctx.post.function_id;
  // 验证表单是否为空
  const isEmpty = ctx.isEmpty(
    {
      function_id: functionId,
      class_id: classId
    },
    ["class_id", "function_id"]
  );
  if (!isEmpty) {
    ctx.error("请完善表单");
    return;
  }
  // 查询是否已配置过
  const aliConfig = await BaaS.Models.pay_alipay
    .query({
      where: { baas_id: baas.id }
    })
    .fetch();
  const dataAdd = {
    function_id: functionId,
    class_id: classId
  };
  Object.assign(dataAdd, { id: aliConfig.id });
  await BaaS.Models.pay_alipay.forge(dataAdd).save();
  ctx.success("提交成功");
});
/**
 *
 * api {get} /home/app/plugins/payAlipay 支付宝支付
 *
 */
router.get("/payAlipay", async (ctx, nex) => {
  const baas = ctx.baas;
  const { id, tradeid, money, redirect } = ctx.query;
  const valid = await ctx.validate({ id: id }, { id: Joi.number().required() });
  if (!valid) {
    ctx.error("参数有误");
    return;
  }
  const notifyUrl = "http://" + ctx.host + "/pay/alipay/notify";
  const showUrl = "http://" + ctx.host;
  const returnUrl =
    "http://" + ctx.host + `/1.0/pay/alipay/return?redirect=${redirect}`;
  const trade = await BaaS.Models.trade
    .query(qb => {
      qb.where("id", "=", id);
      qb.where("status", "=", 0);
    })
    .fetch();
  if (!trade) {
    ctx.error("账单不存在或者已支付");
    return;
  }
  const { partner, key, account } = await BaaS.Models.pay_alipay
    .query({ where: { baas_id: baas.id } })
    .fetch();
  const url = await Payment.createDirectPay(
    partner,
    key,
    account,
    tradeid,
    tradeid,
    money,
    tradeid,
    showUrl,
    notifyUrl,
    returnUrl
  );
  ctx.redirect(url);
});

module.exports = router;
